﻿.. _api-strategy-page:

Strategies
==========

Introduction to Strategies
--------------------------

The strategies aims to define the type of return that will be obtained when making the request. The following is the listing and description of all strategies and their respective returns:

The following values ​​are valid:

    - offers_store
    - offers_category
    - offers_user
    - most_viewed_store
    - most_viewed_categories
    - most_viewed_campaigns
    - most_viewed_user
    - most_viewed_products
    - best_sellers_store
    - best_sellers_categories
    - best_sellers_campaigns
    - best_sellers_user
    - best_sellers_products
    - new_releases_store
    - new_releases_categories
    - new_releases_products
    - new_releases_user
    - click_history
    - navigation_history
    - order_history
    - cart_abandonment
    - bought_together
    - best_choice
    - similar_products
    - who_clicked_also_clicked
    - who_clicked_also_purchased
    - who_purchased_also_purchased
    - promoted_store

Description
-----------

- **offers_store:** returns the main products in the store that have had a price reduction. It is recommended to use this strategy on the home page.

- **offers_category:** returns the main products of the informed category that have had a price reduction. It is recommended to use this strategy on the category page. **It is mandatory to inform the category (or categories)**.

- **offers_user:** returns the main products based on the interest and navigation of a certain user who have had a price reduction. It is recommended to use this strategy on the home page. **It is mandatory to inform the user**.

- **most_viewed_store:** returns the most viewed products in the store. It is recommended to use this strategy on home pages, empty cart, empty search.

- **most_viewed_categories:** returns the most viewed products of an informed category. It is recommended to use this strategy on the category page. **It is mandatory to inform the category (or categories)**.

- **most_viewed_campaigns:** returns the most viewed products of an informed campaign. We recommend using this strategy on the campaign page. If you do not have a campaign page, you should not use this strategy. **It is mandatory inform the campaign (or campaigns)**.

- **most_viewed_user:** returns the most viewed products based on the interest and navigation of a certain user. It is recommended to use this strategy on home pages, empty cart, empty search. **It is mandatory to inform the user**.

- **most_viewed_products:** returns the most viewed products in relation to an informed product. It is recommended to use this strategy on the product detail page. **It is mandatory to inform the product (or products)**.

- **best_sellers_store:** returns the best-selling products in the store. It is recommended to use this strategy on home pages, empty cart, search, empty search.

- **best_sellers_categories:** returns the best-selling products of a given category. It is recommended to use this strategy on the category page. **It is mandatory to inform category (or categories)**.

- **best_sellers_campaigns:** returns the best-selling products of a given informed campaign. We recommend using this strategy on the campaign page. If you do not have a campaign page, you should not use this strategy. **It is mandatory inform the campaign (or campaigns)**.

- **best_sellers_user:** returns the best-selling products based on the interest and navigation of a certain user. It is recommended to use this strategy on home pages, cart, empty cart, search, empty search. **It is mandatory to inform the user**.

- **best_sellers_products:** returns the best-selling products in relation to an informed product. It is recommended to use this strategy on the product detail page. **It is mandatory to inform the product (or products)**.

- **new_releases_store:** returns the releases in the store, that is the products that just entered the catalog of the store. It is recommended to use this strategy on the home page.

- **new_releases_categories:** returns the category releases, that is the products of a certain category that have just entered the catalog of the store. It is recommended to use this strategy on the category page. **It is mandatory to inform the category (or categories)**.

- **new_releases_products:** returns the releases in relation to a given product. It is recommended to use this strategy on the product detail pages, cart. **It is mandatory to inform the product (or products)**.

- **new_releases_user:** returns the releases based on the interest and navigation of a certain user, that is the products that just entered the catalog of the store that were selected specifically for the informed user. It is recommended to use this strategy on home pages, cart, empty cart, search, empty search. **It is mandatory to inform the user**.

- **click_history:** returns the respective recommended products to the last products that were clicked by the user in the store. It is recommended to use this strategy on the home page. **It is mandatory to inform the user**.

- **navigation_history:** returns the last products that were clicked by the user in the store, that is the products that the user entered in the product detail on the website. It is recommended to use this strategy in the home pages, product detail, empty cart, cart, category, search, empty search, internal (FAQs, My Account, About Us, etc). **It is mandatory to inform the user**.

- **order_history:** returns the respective recommended products to the products that were purchased by the user in the store. It is recommended to use this strategy on home pages, empty cart, cart. **It is mandatory to inform the user**.

- **cart_abandonment:** returns the respective recommended products to the products that were abandoned in the store cart by the user. It is recommended to use this strategy on home pages, cart. **It is mandatory to inform the user**.

- **bought_together:** returns the complementary products in relation to an informed product. It is recommended to use this strategy on the product detail page. **It is mandatory to inform the product (or products)**.

- **best_choice:** returns the products you from a certain product informed. It is recommended to use this strategy on the product detail page. However, it is not recommended to use this strategy on the same page as the *similar_products* strategy, because they tend to recommend similar products because both are product-based (which would be the same if they were called on the same page). **It is mandatory to inform the product (or products)**.

- **similar_products:** returns the products considered most similar from a given product. It is recommended to use this strategy on the product detail page. However, it is not recommended to use this strategy on the same page as the *best_choice* strategy, because they tend to recommend similar products because both are product-based (which would be the same if they were called on the same page). **It is mandatory to inform the product (or products)**.

- **who_clicked_also_clicked:** returns the products that are frequently clicked when certain informed product is clicked. It is recommended to use this strategy on the product detail page. **It is mandatory to inform the product (or products)**.

- **who_clicked_also_purchased:** returns the products that are often purchased when certain informed product is clicked. It is recommended to use this strategy on the product detail page. **It is obligatory to inform the product (or products)**.

- **who_purchased_also_purchased:** returns the products that are often purchased along with the purchase of certain product informed. It is recommended to use this strategy on the product detail page. **It is obligatory to inform the product (or products)**.

- **promoted_store:** returns the products determined by the store to be promoted (highlighted) when this strategy is requested.


Properties
----------

Each recommendation strategy requires one or more properties (parameters) to process the request. Following there is a table indicating which parameter should be informed in each strategy and a listing with a brief explanation of all necessary parameters:

.. list-table:: Properties of each type of strategy
   :widths: auto
   :header-rows: 1

   * - Strategy Types
     - Strategy
     - Origin
     - User
     - Products
     - Categories
     - Campaigns
   * - Offers Store
     - ✓
     - ✓
     - 
     - 
     - 
     -
   * - Offers Category
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - Offers User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Most Viewed Store
     - ✓
     - ✓
     - 
     - 
     -
     -
   * - Most Viewed Categories
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - Most Viewed Campaigns
     - ✓
     - ✓
     - 
     - 
     - 
     - ✓
   * - Most Viewed User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Most Viewed Products
     - ✓
     - ✓
     - 
     - ✓
     - 
     -
   * - Best Sellers Store
     - ✓
     - ✓
     - 
     - 
     - 
     -
   * - Best Sellers Categories
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - Best Sellers Campaigns
     - ✓
     - ✓
     - 
     - 
     - 
     - ✓
   * - Best Sellers User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Best Sellers Product
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - New Releases Store
     - ✓
     - ✓
     - 
     - 
     -
     -
   * - New Releases Categories
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - New Releases Campaigns
     - ✓
     - ✓
     - 
     - 
     - 
     - ✓
   * - New Releases User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - New Releases Products
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Click History
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Navigation History
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Order History
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Cart Abandonment
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Bought Together
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Best Choice
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Similar Products
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Who Clicked Also Clicked
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Who Clicked Also Purchased
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Who Purchased Also Purchased
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Promoted Store
     - ✓
     - ✓
     - 
     - 
     - 
     -
