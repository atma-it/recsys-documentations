.. _api-ondemand-request-properties-page:

Recommendation Request Properties
=================================

To perform the Recommendation Request successfully, some properties must be filled in. The recommendations returned will be guided by this information.

The existing properties are:

- **Strategy:** Informs which strategy will be used to generate the recommendation. **This is a required attribute**. For more details, access the section :ref:`Strategies <api-strategy-page>`

- **Origin:** Informs the type of page that requests the recommendation. **This is NOT a required attribute** For more details, access the section :ref:`Origin <api-origin-page>`

- **Channel:** Informs which channel type that requests the recommendation. It is advisable to keep a pattern on calls from the same channel (example: "site", "app"). **NOT a required attribute**.

- **User:** Informs the logged user Id in the application that will serve as the basis for generating the recommendation (example: search for the last products clicked by the user and the like and these). **Required depending on strategy**.

- **Products:** Informs a list with Ids of the products that will serve as a basis for generating the recommendation (example: search for products similar to the ID of the product being viewed). **Required depending on strategy**.

- **Categories:** Informs a list with Ids of the categories that will serve as the basis for generating the recommendation (example: search for top sellers in categories). **Required depending on strategy**.

- **Campaigns:** Informs a list with Ids of the campaigns that will serve as the basis for generating the recommendation (example: nike and adidas soccer tennis campaigns). **Required depending on strategy**.

- **minProducts:** Informs a number and makes it possible to limit a minimum quantity of products recommended in the call return. **NOT a required attribute**.

- **maxProducts:** Informs a number and makes it possible to limit a maximum quantity of products recommended in the call return. **NOT a required attribute**. 

- **mergeRecommendationList:** Informs a Boolean value (true / false) and allows, when the products attribute contains more than one Id, that the respective recommended products for each Id are combined and returned in the same list of recommended products. **NOT a required attribute**.

- **mixMergedRecommendations:** It informs a Boolean value (true / false) and allows, when the products attribute contains more than one Id and when the mergeRecommendationList attribute is true, that in addition to the respective recommended products for each Id are combined and returned in the same list of recommended products, are merged in the return order. **NOT a required attribute**.
