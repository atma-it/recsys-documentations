 .. _api-origin-page:

Origin
======

The origin is intended to inform you what kind of page to ask for the recommendation. By this way, the Biggy Dashboard later provides metrics for e-commerce, as well as the possibility of filtering by page type.

If the page is not informed, it will be registered as "unknown". This information is used in the performance reports, and if not informed all the requests will be stored as "unknown", affecting the evaluation of the recommendations.

The following values ​​are valid:
    - home
    - product
    - product_unavailable
    - category
    - campaign
    - cart
    - empty_cart
    - checkout
    - confirmation
    - search
    - empty_search
    - not_found
    - order_list
    - order_detail
    - internals
    - unknown
