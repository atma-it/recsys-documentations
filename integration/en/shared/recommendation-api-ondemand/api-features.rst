Recommendation Characteristics
==============================

 .. _api-ondemand-features-page:
 
The recommendation provided by Biggy allows you to approach users in a variety of ways, each with its own particularity and appropriate for different pages of your e-commerce.

Among the various possibilities, it is possible to obtain recommendations such as:

- Most popular products from the store or category;
- Complementary products to a product;
- Products similar to a product;
- Products left on the cart by the user, with a list of similar products;
- Products recently clicked by the user, with a list of similar products;
- Products recently purchased by the user, with a list of complementary products;
- Most viewed products of categories that the user has shown most interest in the past;
- Products that are news of the categories that the user has shown most interest in the past;
