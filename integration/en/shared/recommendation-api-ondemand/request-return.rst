.. _api-ondemand-request-return-page:

Recommendation Request Return
=============================

The return of a Recommendation Request call is a list of objects, where each object is formed by 4 (four) properties, which are:
    1. Base Ids
    2. Base Items
    3. Recommendation Ids
    4. Recommendation Items


1. Base Ids
-----------

Returns a List of *String* that contains the **ids** of the entity used as **bases** for the request of the recommendation, which can be product id, category id and campaign id. It should be noted that this field can be null when there is no base id (example: recommend request with strategies based on the store).


- *Example 1:* "best sellers campaigns" strategy will have the ids of the campaigns informed in the baseIds list.

- *Example 2:* "best sellers categories" strategy will have the ids of the categories reported in the baseIds list.

- *Example 3:* id of the product entered as parameter on the product detail page.

- *Example 4:* ids of products clicked by the user used in the "navigation history" strategy.


2. Base Items
-------------

Returns a List of **Recommendation Items** used as a basis for recommendation. Each **recommendation item** contains all base product information. This field can be empty when there is no base product (example: "new releases category" strategy).

- *Example 1:* product entered as a parameter on the product detail page.

- *Example 2:* a list of products clicked by the user used in the "navigation history" strategy.


3. Recommendation Ids
---------------------

Returns a List of *String* that contains the **ids** of the **Recommendation Items**, which would be the ids of the recommended products.

- *Example:* "best sellers campaigns" strategy will return a list with Ids of best selling recommendation items for the base campaign.


4. Recommendation Items
-----------------------

Returns a List of **Recommendation Items**, which would be the recommended products.

- *Example:* "best sellers campaigns" strategy will return a list with best selling recommendation items for the base campaign.


.. note:: The completion of the **baseIds** or **baseItems**, **recommendationIds** or **recommendationItems** varies depending on the return configuration configured by Biggy. If you want to mount recommendations using your database data, for example, you can disable recommendationItems return to decrease the amount of data trafficked by returning only the product Ids that should be recommended.


Recommendation Item
-------------------

The **Recommendation Item** is an object contained in the *Base Items* and *Recommendation Items* lists (since both have the same return structure). Each recommendation item has the following attributes:

- **productId** containing the product ID.
- **score** containing the score obtained by the recommendation (if any). Example: best-choice strategy returns a % indicating how relevant the recommendation is.
- **specs** containing the specification list for this product. Example: all colors and sizes of running shoes; all the flavors of a whey protein.
- **offers** containing the list of product SKUs.

The objects in the list of specs aims to present the specifications of the products using the following structure:

- **id** containing the ID of the spec.
- **label** containing the label / spec name.
- **type** containing spec type. The possible values ​​are:
    - color
    - size
    - flavor
- **subSpecs** containing possible daughter specifications (if any). Example: Blue spec type color tennis has subspecs type size 37, 38, 39, 40 and 41.

Objects in the **offers** list can be considered the main return object because they detail all product SKUs. For this the following structure is used:

- **offerId** containing the offer ID.
- **sku** containing the offer SKU.
- **name** containing the offer name.
- **url** containing the offer url.
- **imageUrl** containing the offer image url.
- **currencySymbol** containing the currency symbol as text following the formatting of the local currency.
- **price** containing the offer price as text following the formatting of the local currency.
- **oldPrice** containing the "offer" price as text following the formatting of the local currency.
- **priceValue** containing the offer price as a numeric.
- **installment** containing the installment of the offer.
- **hasDiscount** informing you whether or not the offer has a discount.
- **discountPercentage** informing the percentage of the offer (if any).
- **specs** informing the SKU's unique specifications (example: "spec type color" blue running shoe and "spec type size" 41).
