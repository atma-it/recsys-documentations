﻿B-API On Demand
===============

The **b-api on demand** provides customized recommendations with a variety of **strategies** (that will be described bellow), serving the demand in diverse scenarios, from pages in mobile applications to systems in physical stores.

For more details on the return possibilities, access the section: :ref:`Recommendation Characteristics <api-ondemand-features-page>`

This API consists of three main methods described below:
    - Recommendation Request
    - Recommendation View
    - Recommendation Click
    
Recommendation Request
----------------------

The Recommendation Request is responsible for submitting all the data that will be required for the Recommendation API and is mandatory on all calls.

An **unique** *endpoint* attend all possible recommendations, with the following structure:

.. code-block:: json

    POST http://<domain-biggy-api>/rec-api/v1/<store-id>/ondemand/<strategy>
    
    {
        "origin": "product", 
        "channel": "app",
        "products": [
            "4567"
        ], 
        "user": "1234",
        "minProducts": 2,
        "maxProducts": 12,
        "mergeRecommendationList": true,
        "mixMergedRecommendations": false
    }

    
For more details about the properties (parameters), access the section :ref:`Recommendation Request Properties <api-ondemand-request-properties-page>`

By this way, the request for **b-api on demand** is performed and a return JSON composed of 4 lists is obtained:

    1. baseIds
    2. baseItems
    3. recommendationIds
    4. recommendationItems
    
For more details about the return, access the section :ref:`Recommendation Request Return <api-ondemand-request-return-page>` 

The following is an example of the callback of the recommendation:

.. code-block:: json

    [
    {
        "baseIds": [
            "123456"
        ],
        "baseItems": [
            {
                "productId": "123456",
                "offers": [
                    {
                        "offerId": "1234560O0395",
                        "originalProductId": "123456",
                        "sku": "1234560O0395",
                        "name": "Chuteira de Campo Dry Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-dry-adulto.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/12345600.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "oldPrice": "389,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO",
                                "type": "COLOR",
                                "offerId": "1234560O0395",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "1234560O0395",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "Dry",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "123456080440",
                        "originalProductId": "123456",
                        "sku": "123456080440",
                        "name": "Chuteira de Campo adidas X 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-3-fg-adulto-123456.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/12345608.jpg",
                        "secondaryImageUrl": "",
                        "price": "269,99",
                        "oldPrice": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "27",
                            "count": "10"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 22,
                        "specs": [
                            {
                                "id": "08",
                                "label": "AMARELO",
                                "type": "COLOR",
                                "offerId": "123456080440",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "044",
                                        "label": "44",
                                        "type": "SIZE",
                                        "offerId": "123456080440",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "123456080402",
                        "originalProductId": "123456",
                        "sku": "123456080402",
                        "name": "Chuteira de Campo adidas X 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-3-fg-adulto-123456.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/12345608.jpg",
                        "secondaryImageUrl": "",
                        "price": "269,99",
                        "oldPrice": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "27",
                            "count": "10"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 22,
                        "specs": [
                            {
                                "id": "08",
                                "label": "AMARELO",
                                "type": "COLOR",
                                "offerId": "123456080402",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "040",
                                        "label": "40",
                                        "type": "SIZE",
                                        "offerId": "123456080402",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "1234560O0418",
                        "originalProductId": "123456",
                        "sku": "1234560O0418",
                        "name": "Chuteira de Campo adidas X 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-3-fg-adulto-123456.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/1234560O.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "oldPrice": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "1234560O0418",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "041",
                                        "label": "41",
                                        "type": "SIZE",
                                        "offerId": "1234560O0418",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    }
                ],
                "specs": [
                    {
                        "id": "08",
                        "label": "AMARELO",
                        "type": "COLOR",
                        "offerId": "123456080402",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "040",
                                "label": "40",
                                "type": "SIZE",
                                "offerId": "123456080402",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "123456080440",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    },
                    {
                        "id": "0O",
                        "label": "PRETO/LARANJA ESC",
                        "type": "COLOR",
                        "offerId": "1234560O0395",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "1234560O0395",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "040",
                                "label": "40",
                                "type": "SIZE",
                                "offerId": "1234560O0401",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "041",
                                "label": "41",
                                "type": "SIZE",
                                "offerId": "1234560O0418",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "042",
                                "label": "42",
                                "type": "SIZE",
                                "offerId": "1234560O0425",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "043",
                                "label": "43",
                                "type": "SIZE",
                                "offerId": "1234560O0432",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "1234560O0449",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    }
                ]
            }
        ],
        "recommendationIds": [
            "987654",
            "345602"
        ],
        "recommendationItems": [
            {
                "productId": "987654",
                "score": 26,
                "offers": [
                    {
                        "offerId": "987654J40444",
                        "originalProductId": "987654",
                        "sku": "987654J40444",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 26,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40444",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "044",
                                        "label": "44",
                                        "type": "SIZE",
                                        "offerId": "987654J40444",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "987654J40413",
                        "originalProductId": "987654",
                        "sku": "987654J40413",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 860712,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40413",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "041",
                                        "label": "41",
                                        "type": "SIZE",
                                        "offerId": "987654J40413",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "987654J40390",
                        "originalProductId": "987654",
                        "sku": "987654J40390",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 860712,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40390",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "987654J40390",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "987654J40420",
                        "originalProductId": "987654",
                        "sku": "987654J40420",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 860712,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40420",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "042",
                                        "label": "42",
                                        "type": "SIZE",
                                        "offerId": "987654J40420",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    }
                ],
                "specs": [
                    {
                        "id": "J4",
                        "label": "LARANJA ESC/PRETO",
                        "type": "COLOR",
                        "offerId": "987654J40390",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "987654J40390",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "041",
                                "label": "41",
                                "type": "SIZE",
                                "offerId": "987654J40413",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "042",
                                "label": "42",
                                "type": "SIZE",
                                "offerId": "987654J40420",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "987654J40444",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    }
                ]
            },
            {
                "productId": "345602",
                "score": 25,
                "offers": [
                    {
                        "offerId": "3456020O0416",
                        "originalProductId": "345602",
                        "sku": "3456020O0416",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 25,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0416",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "041",
                                        "label": "41",
                                        "type": "SIZE",
                                        "offerId": "3456020O0416",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0447",
                        "originalProductId": "345602",
                        "sku": "3456020O0447",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0447",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "044",
                                        "label": "44",
                                        "type": "SIZE",
                                        "offerId": "3456020O0447",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0393",
                        "originalProductId": "345602",
                        "sku": "3456020O0393",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0393",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "3456020O0393",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0409",
                        "originalProductId": "345602",
                        "sku": "3456020O0409",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0409",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "040",
                                        "label": "40",
                                        "type": "SIZE",
                                        "offerId": "3456020O0409",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0386",
                        "originalProductId": "345602",
                        "sku": "3456020O0386",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0386",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "038",
                                        "label": "38",
                                        "type": "SIZE",
                                        "offerId": "3456020O0386",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0430",
                        "originalProductId": "345602",
                        "sku": "3456020O0430",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0430",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "043",
                                        "label": "43",
                                        "type": "SIZE",
                                        "offerId": "3456020O0430",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "345602080387",
                        "originalProductId": "345602",
                        "sku": "345602080387",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/34560208.jpg",
                        "secondaryImageUrl": "",
                        "price": "179,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.71",
                            "count": "7"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 21,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "08",
                                "label": "AMARELO",
                                "type": "COLOR",
                                "offerId": "345602080387",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "038",
                                        "label": "38",
                                        "type": "SIZE",
                                        "offerId": "345602080387",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "345602460394",
                        "originalProductId": "345602",
                        "sku": "345602460394",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/34560246.jpg",
                        "secondaryImageUrl": "",
                        "price": "179,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.71",
                            "count": "7"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 21,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "46",
                                "label": "BRANCO/CINZA",
                                "type": "COLOR",
                                "offerId": "345602460394",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "345602460394",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0423",
                        "originalProductId": "345602",
                        "sku": "3456020O0423",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0423",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "042",
                                        "label": "42",
                                        "type": "SIZE",
                                        "offerId": "3456020O0423",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    }
                ],
                "specs": [
                    {
                        "id": "08",
                        "label": "AMARELO",
                        "type": "COLOR",
                        "offerId": "345602080387",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "038",
                                "label": "38",
                                "type": "SIZE",
                                "offerId": "345602080387",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    },
                    {
                        "id": "0O",
                        "label": "PRETO/LARANJA ESC",
                        "type": "COLOR",
                        "offerId": "3456020O0423",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "038",
                                "label": "38",
                                "type": "SIZE",
                                "offerId": "3456020O0386",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "3456020O0393",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "040",
                                "label": "40",
                                "type": "SIZE",
                                "offerId": "3456020O0409",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "041",
                                "label": "41",
                                "type": "SIZE",
                                "offerId": "3456020O0416",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "042",
                                "label": "42",
                                "type": "SIZE",
                                "offerId": "3456020O0423",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "043",
                                "label": "43",
                                "type": "SIZE",
                                "offerId": "3456020O0430",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "3456020O0447",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    },
                    {
                        "id": "46",
                        "label": "BRANCO/CINZA",
                        "type": "COLOR",
                        "offerId": "345602460394",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "345602460394",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    }
                ]
            }
        ]
    }
    ]
  

Recommendation View
-------------------

The Recommendation View allows you to evaluate through the Biggy Dashboard the performance of the recommendation that you requested in relation to its visualization. The goal is to make this call when the user "sees" the recommendation.

Following is an example of the Recommendation View call:

.. code-block:: json

    POST http://<domain-biggy-api>/rec-api/v1/<store-id>/ondemand/<strategy>/view
    
    {
        "origin": "product", 
        "session": "session123456"
    }

.. note:: The *strategy* and *origin* attributes follow the same existing structure for the Recommendation Request. For more details, access the sections :ref:`Strategies <api-strategy-page>` and :ref:`Origin <api-origin-page>`


Recommendation Click
--------------------

Recommendation Click allows you to evaluate through the Biggy Dashboard the performance of the requested recommendation regarding the triggering of click events. The purpose is to call this method when the user "clicks" on some product of the recommendation.

Following is an example of the Recommended Click call:

.. code-block:: json

    POST http://<domain-biggy-api>/rec-api/v1/<store-id>/ondemand/<strategy>/click
    
    {
        "origin": "product", 
        "session": "session123456",
        "productId": "123456"
    }

.. note:: The *strategy* and *origin* attributes follow the same existing structure for the Recommendation Request. For more details, access the sections :ref:`Strategies <api-strategy-page>` and :ref:`Origin <api-origin-page>`
