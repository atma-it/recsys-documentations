﻿B-API Campaign
==============

The **b-api campaign** has two APIs that must be injected directly into your HTML code to create the recommendation store front. These APIs return the *image* and *link* of the recommended product, making it easy to integrate into mail marketings and blogs.

Example of using b-api campaign:

.. code-block:: html

    <a href="http://<domain-biggy-api>/rec-api/v1/<store-id>/link/0?email=<email-target-user>&uniqueId=<unique-id>">
        <img src="http://<domain-biggy-api>/rec-api/v1/<storeId>/image/0?email=<email-target-user>&templateId=<templateid>&uniqueId=<unique-id>">
    </a>
    

Example with the values ​​filled for a store front with 3 products:

.. code-block:: html
    
    <a href="http://api.biggylabs.com/rec-api/v1/123456/link/0?email=alan@biggy.com.br&uniqueId=natal_27012016">
        <img src=" http://api.biggylabs.com/rec-api/v1/123456/image/0?email=alan@biggy.com.br&templateId=987654&uniqueId=natal_27012016">
    </a>
    <a href="http://api.biggylabs.com/rec-api/v1/123456/link/1?email=alan@biggy.com.br&uniqueId=natal_27012016">
        <img src=" http://api.biggylabs.com/rec-api/v1/123456/image/1?email=alan@biggy.com.br&templateId=987654&uniqueId=natal_27012016">
    </a>
    <a href="http://api.biggylabs.com/rec-api/v1/123456/link/2?email=alan@biggy.com.br&uniqueId=natal_27012016">
        <img src=" http://api.biggylabs.com/rec-api/v1/123456/image/2?email=alan@biggy.com.br&templateId=987654&uniqueId=natal_27012016">
    </a>

.. note:: Note that for each product to be displayed, the index call (**0**, **1**, **2** ...) is subject to variation.

The <a> tag populates the "href" attribute using the *endpoint* **link**.

The <img> tag populates the "src" attribute using the *endpoint* **image**.

The following parameters are required in the *endpoints* **link** and **image**:

- **storeId:** unique store identifier.
- **email:** target user email.
- **uniqueId:** unique identifier of your email campaign. We suggest using the campaign name concatenated with the date of submission (eg, natal_27012017). If the campaign does not have a name, use only the date of submission (note: this field is only used to export performance reports from the Biggy administrative console).

In the *endpoint* **image** it is possible to inform which template should be presented in the store front using the parameter **templateId**, which can be obtained through the administrative console. If the templateId is not informed, a default template will be used in the presentation of the store front.

In addition, it is possible to create and edit new  store front templates and configure attributes such as: location of price, font size and color and size of images, background color etc. You can create as many templates as you like and use them on demand (example: email marketing with a different design).

Another parameter that can be used is **random** (true/false - optional). It is used if it is necessary to make a variation in the order of the customer's recommendations.

Other optional parameters that can be passed in the *endpoints*, and must be equal for both **image** and **link** *endpoint* of the same recommendation:

- **strategy:** Strategy to be used for recommendations, list of strategies available below.
- **categoryId:** If the strategy is of recommendation for categories, passing the id of a category, the b-api return will be of products of the informed category. If you do not have recommendation for the reported category, return the recommendation for the same strategy but open for all products in the store.
- **productId:** If the strategy is recommended for products, passing the id of a product, the b-api return will be of products recommended based on the product passed as base. If the strategy is of recommendation for categories, the b-api return will be products of the same category as the last product.
- **campaignId:** If the strategy is recommended for campaigns, passing the id of a campaign, the b-api return will be the products of the campaign informed.
- **tags:** A comma-separated list of tags, the b-api return will be for products that have at least one of the reported tags.

If the past strategy is recommended for categories, and only the user's email is passed, the b-api return will be from products of the last category accessed by the user. If none of the recommendation paths explained above applies, the b-api return will follow the default path, with recommendations based on user interest.

Example of using b-api campaign with another recommendation path:

.. code-block:: html
    
    <a href="http://<domain-biggy-api>/rec-api/v1/<store-id>/link/0?productId=<id-produto>&uniqueId=<unique-id>&strategy=MVW">
        <img src="http://<domain-biggy-api>/rec-api/v1/<storeId>/image/0?productId=<id-produto>&templateId=<template-id>&uniqueId=<unique-id>&strategy=MVW">
    </a>

Following there is a table with the value that must be passed in the *strategy* parameter to use them and what are their possible inputs:

.. list-table:: Strategies for the b-api campaign
   :widths: auto
   :header-rows: 1

   * - Strategy Name
     - Strategy     
     - User
     - Category
     - Product
     - Campaign
   * - Best Sellers
     - BST
     - ✓
     - ✓
     - ✓
     - ✓
   * - Most Viewed
     - MVW
     - ✓
     - ✓
     - ✓
     - ✓
   * - Last Price Reduction
     - LPR
     - ✓
     - ✓
     - ✓
     - ✓
   * - New Releases
     - NEW
     - ✓
     - ✓
     - ✓
     - ✓
   * - Similar Products
     - SCO
     - 
     - 
     - ✓
     - 
   * - Best Choice
     - BCH
     - 
     - 
     - ✓
     - 
   * - Bought Together
     - GCMP
     - 
     - 
     - ✓
     - 
   * - User Navigation
     - NAV
     - ✓
     - 
     - 
     - 
     

