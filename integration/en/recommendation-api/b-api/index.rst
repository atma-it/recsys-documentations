﻿B-API
=====

In order to integrate in different platforms and to allow customization in different contexts, Biggy offers **b-api**, a service of recommendation through API that has two approaches:

 - **B-API Campaign:** APIs that return customized per-user recommendations ready to integrate into email marketing and blogging, with easy-to-use window assembly calls without the need for IT involvement.
 
 - **B-API On Demand:** APIs ready to be consumed in any context, from mobile applications to physical store systems. The recommendations take into account the customer, category, product and store.
