﻿First Steps
===========

Biggy's recommendation platform has 3 products: **b-mail**, **b-api** and **b-front**. Regardless of which product will be used, Biggy integration takes place in 2 main steps:

 1. Layout definition and construction;
 2. Metadata implementation.
 
The *Layout* topic (detailed in the "Development" section) discusses how to implement the recommendation layout presentation for b-mail, b-api, and b-front.
Biggy provides a development team to implement the full integration and recommendation metadata and layout for b-mail, b-api and b-front. Normally, between 3 and 5 weeks are required to feed Biggy's databases with your e-commerce information and to develop all the necessary layouts.

The *Metadata* topic covers integrating all of your e-commerce pageviews with Biggy through a REST API. This integration allows the behavioral analysis of users and the recommendation mechanism processes with high performance and assertiveness.
