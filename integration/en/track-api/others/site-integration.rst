﻿Site Integration
================

As mentioned previously, for the site integration, there are three possible different approaches:

1. Platform Plugin
------------------

Biggy provides a plugin for the major e-commerce platforms, generally compatible with the latest versions.

Check with our team if there is a plugin available for your platform compatible with the version used in your e-commerce.


2. Implementation in the source code of your e-commerce
-------------------------------------------------------

Biggy provides a development team to implement metadata and API calls directly into the source code of your e-commerce, following exactly what was presented in this document.

If there are any security restrictions to access your e-commerce source code, Biggy also provides a team to answer any questions you may have about the implementation.


3. Google Tag Manager (GTM)
---------------------------

The Google Tag Manager (GTM) tool can be used to do the integration.

Using the **data layer** concept, you can create **macros** within the GTM to populate the Biggy metadata **as long as all the information** already exists in the *data layer*. All attributes must be filled in exactly as described in this documentation.

To call the API, simply create a **tag** inside the GTM and ensure that **all pages** will render it. This *tag* must have the code provided in the "Recsys Script" session and must be executed after the metadata is loaded by the *data layer* and *macros*.
