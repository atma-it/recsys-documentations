﻿Past Data Injection
===================

Past data injection is a technique applied to a new integrated Biggy store. This process is intended to integrate data passed prior to integration with Biggy. By this way, it will be possible to use information that could not be captured by the integration because they had already happened. The result of this is the enrichment of the database and the improvement of the performance of the recommendation.

For more information, consult the Biggy team.
