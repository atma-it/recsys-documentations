﻿B-Front Integration
===================

Stages
------

Integration with the Biggy service called **b-front** consists of two main steps:

1. Import the *loader.js* script into the e-commerce HTML *header*;

2. Create and position the <div> tags in areas and pages where the front of the store will be displayed. The available pages are:
    - home
    - product
    - detail
    - cart
    - category
    - search
    - checkout
    - confirmation
    
Example:

.. code-block:: html

    <head>
        <script src='<LOADER_JS_URL>' biggy-key="'<BIGGY_KEY>'"></script>
    </head>
    
    <body>
        <div id="biggy-one"   biggy-area="one"></div>
        <div id="biggy-two"   biggy-area="two"></div>
        <div id="biggy-three" biggy-area="three"></div>
        <div id="biggy-four"  biggy-area="four"></div>
        <div id="biggy-five"  biggy-area="five"></div>
    </body>
    


After performing the two steps above, **b-front** will be available and will be rendered in the store fronts and required pages.


Store Front Script
------------------

If the metadata is mounted in the body, it is indicated to call with autorun enabled.

For special stores with async or ajax metadata, you must disable autorun and call manually as soon as metadata is available.

.. code-block:: html

    <script src="'<LOADER_JS_URL>'" biggy-key="'<BIGGY_KEY>'" biggy-autorun="false"></script>
    

.. note:: The settings can be entered in another <script> block. The example below follows.

.. code-block:: html

    <script>
        window.biggyFrontKey = '<BIGGY_KEY>';
        window.biggyFrontAutorun = false;
    </script>

    <script src="'<LOADER_JS_URL>'"></script>
    
If you choose to inform the settings in another block, to make the call manually, simply execute the following code:

.. code-block:: html

    window.biggyFrontRunAsync();
