﻿Layout Development
==================

Layout and presentation are made by Biggy web developers using your e-commerce layout as a base/reference and thus maintains compliance between the two.

As observation, **b-mail**, **b-api** and **b-front** have some singularities when it comes to layout implementation:

- **b-mail:** development of all HTMLs for each email campaign that will be sent to your user. The Biggy team ensures responsive HTML that will be presented with high quality on the leading webmail services on the market.

- **b-api:** this service has two types:
    - *b-api campaign*: the development of an image builder that uses a template to create product images that will be available through our recommendation API (generally used in email marketing).
    - *b-api on demand:* there is no layout development for this service, since it returns only the recommendations information (without templates).
    
- **b-front:** development of all HTML that will be rendered in e-commerce (both www and m. versions);

Basically, meetings with both teams for layout definitions are required to start development.
