﻿Scripts and metadata Integration 
================================

Metadata integration should occur on both sites (www and m.), as well as mobile applications (Android and/or IOS), if they exist.

All site or app page views should call the Biggy REST API to ensure real-time ingestion of data. The data sent in this integration is called **metadata** and will be detailed later.

For site integration, we can use one of three different approaches:

1. Plugin your ecommerce platform (example: VTEX, Magento, etc.), when available;

2. Implementation in the source code of your electronic commerce;

3. Google Tag Manager (GTM).


For application integration for mobile devices, Biggy offers an SDK and Framework for Android and IOS platforms, respectively.

Regardless of the approach, Biggy provides a team of developers to assist you in the transition and implementation of data integration and layout definition.

In summary, these integration methods perform two essential tasks:

- Insert a script that calls Biggy API;
- Create the variable called "metadados_recsys" on each e-commerce page (detailed later). It is worth mentioning that this variable has a unique structure for each type of page, such as: home, product detail, cart, category, checkout and confirmation.
