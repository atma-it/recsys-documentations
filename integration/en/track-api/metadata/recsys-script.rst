﻿Recsys Script
=============

All e-commerce web pages must import the Biggy script responsible for encapsulating calls to our tracking API.

There are two ways to perform the script import and hence the call to our API:

- **Automatic:** only used if at the time of script import there is a guarantee that the metadata is already created and will not change. In this scenario, you simply import the recsys.js script, which will automatically call the API at the end of the script.

- **Manual:** used when greater control over API calls is required. In this case, the script prepares the RecSys JavaScript object that will be used by your site at appropriate times. In this scenario, simply import the script **recsys.js** and make calls using the *RecSys.Tracker* method passing as a parameter the attributes: _recsysa.id, _recsysa.host, _recsysa.options. These attributes will be exemplified later.

.. note:: To ensure full integration between e-commerce and Biggy's solution, importing the script must occur on all pages of the web application using the code below:

.. code-block:: javascript

    (function () { 
        var sid = '<ADD_STORE_ID>',
            host = '<ADD_API_HOST>',
            options = {};         
            
        _recsysa = { 
            id: sid, 
            host: host, 
            options: options 
            }; 
        
        metadados_recsys = window.metadados_recsys || {};
        var script = document. createElement('script');
        script.src = '<ADD_SCRIPT_URL>';
        script.async = true; 
        
        var firstScript = document.getElementsByTagName('script') [0];
        firstScript.parentNode.insertBefore(script, firstScript); 
    }()); 


.. note:: The fields **'<ADD_STORE_ID>'**, **'<ADD_API_HOST>'** and **'<ADD_SCRIPT_URL>'** will be sent by the Biggy team during the integration process.
