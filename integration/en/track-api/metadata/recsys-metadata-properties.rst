﻿Metadata Properties
===================

The global variable **metadados_recsys** must be implemented in the e-commerce web pages. The variable has some properties that must be reported depending on which page the **metadados_recsys** is from.

Page
----

The property called **page** must be inserted in **metadados_recsys**. *Must be included on all pages.*

This property contains information about user navigation and should be created as the following example:

.. list-table:: Properties of type *page*
   :widths: auto
   :header-rows: 1

   * - Name
     - Key
     - Type
     - Description
   * - Page Name*
     - name
     - String
     - Describes which page the user is currently browsing. Possible values: home, product, product_unavailable, category, campaign, cart, empty_cart, checkout, confirmation, search, empty_search, not_found, order_list, order_detail, internals, unknown
     
Example:

.. code-block:: json

    page: {
        name: "home"
    }


User
----

The property called **user** must be inserted in **metadados_recsys**. 

*Must be included on all pages.*

If the user is not logged in, the object can be omitted.

This property contains information about the user who is browsing the site and should be created as follows:


.. list-table:: Properties of type *user*  
   :widths: auto
   :header-rows: 1

   * - Field name
     - Key
     - Type
     - Description
   * - User Id*
     - id
     - String
     - The code that the site uses internally to identify this user
   * - Full name*
     - name
     - String
     - The user full name
   * - Email*
     - email
     - String
     - The user email
   * - Allow email*
     - allow_mail_marketing
     - Boolean
     - Informs if the user authorizes the receipt of emails from the store. Possible values: *true* or *false*
   * - User name
     - username
     - String
     - Name used to identify the user in the system
   * - Nickname
     - nickname
     - String
     - Treatment name used by the user. How would you like to be called
   * - Date of birth
     - birthday
     - String
     - User's date of birth. The field must obey the format yyyyMMdd. Example: 19820417
   * - User language
     - language
     - String
     - User language preference in format compatible with IETF. Example: en-US, pt-BR
   * - Tags
     - tags
     - String Array
     - User tags are the information transmitted from each user in a linear and non-hierarchical way
   * - Zip Code
     - zipcode
     - String
     - User's zip code
   * - Country
     - country
     - String
     - User country following the standard two letter *ISO 31661 alpha 2*. Example: BR, US, PT, ES, etc.
   * - Gennder
     - gender
     - String
     - User gender. Possible values: M (male) or F (female)
   * - National Identification
     - document_id
     - String
     - National identification of the user. Example: National Registry, Social Security Number, etc.
   * - Additional Information
     - extra_info
     - Object
     - Map with additional information that does not match any other fields of this user object
     
Example:

.. code-block:: json

    user: {
        id: "1",
        name: "Jose Rodolfo",
        email: "joserodolfo@email.com",
        language: "pt-BR",
        country: "BR",
        gender: "M",
        allow_mail_marketing: true
    }


Categories
----------

The property called **categories** must be inserted in **metadados_recsys**. 

Must be included in category pages. These pages represent the e-commerce departments.

.. note:: It is important that the products, belonging to a subcategory, have the same category structure as the page that is included.

.. list-table:: Properties of type *categories*
   :widths: auto
   :header-rows: 1

   * - Name
     - Key
     - Type
     - Description
   * - Category Id*
     - id
     - String
     - Unique identifier for the category
   * - Category name*
     - name
     - String
     - Category name
   * - Parent Categories
     - parents
     - Categories Array
     - List of objects of type Categories that are the parent categories of the informed category
     
Example:

.. code-block:: json

    categories :[
    {
        id: "123",
        name: "Tênis Casual",
        parents" :[
            "321"
        ],
    },
    {
        id: "321",
        name: "Tênis"
    }
    ]


Search
------

The property called **search** must be inserted in **metadados_recsys**. 

Must be included in **search results** pages of e-commerce and should be created as follows:

.. list-table:: Properties of type *search*
   :widths: auto
   :header-rows: 1

   * - Name
     - Key
     - Type
     - Description
   * - Search Term*
     - query
     - String
     - Term used in the search
   * - Returned products
     - items
     - Products Array
     - List of objects of type Product, which are the products returned in the search. In this case, only the product Id is required.
     
Example:

.. code-block:: json

    search: {
        query: "tenis e camiseta",
        items: [
        {
            id: "1"
        },
        {
            id: "2"
        }
        ]
    }


Product
-------

The property called **product** must be inserted in **metadados_recsys**. 

This property contains information about the product that the user is viewing on the web page.

*Must be included on product detail page.*

There are different types of information for different products depending on their category or any particularity. To organize it, we divided our structure into different sessions:
    - Essential items
    - Specifications (specs)
    - SKUs
    - Kit Product.

In this arrangement, all properties are included only in the variable **product**.

.. note:: All mandatory properties are marked with "*"

- **Essential items:** The following table lists all the essential properties supported. You should fill in as much information as possible.

.. list-table:: Properties of type *product* classified as Essential items
   :widths: auto
   :header-rows: 1
   
   * - Name
     - Key
     - Type
     - Description
   * - Product Id*
     - id
     - String
     - Unique identifier of the product inside the store
   * - Name*
     - name
     - String
     - Complete product name
   * - Product Url*
     - url
     - String
     - Complete address of product. Informed without *http* or *https* protocol
   * - Images*
     - images
     - Object Image
     - A map with all images of the product. Keys: *weight x high*. Values: image url. The default image should be set into **default** property
   * - Availability*
     - status
     - String
     - Informs the availability (status) of the product in the store. Possible values: *available* or *unavailable*     
   * - Categories*
     - categories
     - Categories Array
     - Set of categories that the product belongs to
   * - Price*
     - price
     - Number
     - Informs the price of the product in the store. This field must be filled using the period (.) As the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Old price
     - old_price
     - Number
     - Informs the old price of the product in the store. This field must be filled using the period (.) As the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Description
     - description
     - String
     - Full description of the product. Must be a text without HTML tags
   * - Tags
     - tags
     - Object Array
     - Set of tags that the product contains
   * - Brand
     - brand
     - String
     - Product brand
   * - Publication date
     - published
     - String
     - Date which product was added on the site. The field must obey the formarto yyyyMMdd. Example: 20160217
   * - Product specifications
     - specs
     - Object Spec
     - Declaration of all specifications that the product may have. More details on the topic "Specifications"
   * - SKUs
     - skus
     - Sku Array
     - List of product SKUs and their characteristics. More details on the topic "Skus"
   * - Measurement unit
     - unit
     - String
     - Measurement unit used in the product. Inform this and this only for products that contain fractionated quantities
   * - Kit products
     - kit_products
     - Product Array
     - List of products belonging to the kit
   * - Additional Information
     - extra_info
     - Object
     - Map with additional information that does not match any other field of this product object. Keys: extra name. Values: extra values. Examples: *new*, *promo*, *free_shipping*, *black_friday*, etc.
     
Example:

.. code-block:: json

    product: {
        id: "2",
        name: "Tênis 2 - Feminino",
        description: "...",
        url": "www.ecommerce.com.br/tenis2feminino", 
        images: {
            default: "/180x180/2.jpg"
        }, 
        status: "available",
        old_price: 499.99,   
        price: 379.99,
        installment: {
            count: 12,
            price: 31.67
        },
        tags: [
        {
            id: "calçados",
            name: "calçados"
        },       
        {
            id: "tênis",
            name: "tênis"
        }
        ],
        "extra_info": {
            "new":false,
            "promo":true,
        },
        specs: {…},
        skus: [...]
    }
    
- **Specifications (specs):** Each product can have its own collection of features with dynamic values, called specifications. It must be declared in the **specs** property within the **product** object. The following table presents all the properties of the specifications.

.. list-table:: Specification properties (*specs*) of a *product*
   :widths: auto
   :header-rows: 1

   * - Name
     - Key
     - Type
     - Description
   * - Color
     - color
     - String
     - Identifier of possible product colors (or your SKU)
   * - Size
     - size
     - String
     - Identifier of possible product sizes (or your SKU)
   * - Voltage
     - voltage
     - String
     - Identifier of possible product voltages (or your SKU)
   * - Media type
     - media
     - String
     - Identifier of possible media types of the product (or your SKU). Possible values: "paper", "e-book", "DVD", etc.
     
Example:

.. code-block:: json

    "specs": {
        "cor": [
        {
            "id":"58",
            "label":"cinza",
            "url":"www.ecommerce.com.br/tenis2feminino?cor=58",
            "images": {
                "default":"/180x180/258.jpg"
            }
        },
        {
            "id":"6S",
            "label":"azul",
            "url":"www.ecommerce.com.br/tenis2feminino?cor=6S",
            "images": {
                "default":"/180x180/26S.jpg"
            }
        }
        ],
        "tamanho": [
            {
                "id":"034",
                "label":"34"
            },
            {
                "id":"035",
                "label":"35"
            }
        ]
        }
        
- **SKUs:** The product may have a **list of SKUs** representing:
    
    1. Combinations of specifications defined in the product (combinations of **specs**);
    2. Price details, such as price, old price and different installment.
    
The following properties can be declared in the product and SKU also:
    - name
    - url
    - description
    - images
    - images_ssl
    - status
    - ean_code

.. note:: In case both are declared, the SKU property has a higher priority than product property.

The following table lists all the properties of a SKU.

.. list-table:: Properties of a *product* SKU
   :widths: auto
   :header-rows: 1

   * - Name
     - Key
     - Type
     - Description
   * - SKU*
     - sku
     - String
     - SKU code representing the combination of the specifications defined in that object
   * - SKU Specifications*
     - specs
     - Spec Object
     - Map with SKU specification identifiers
   * - Price*
     - price
     - Number
     - Informs the product SKU price in the store. This field must be filled using the period (.) As the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Old price
     - old_price
     - Number
     - Informs the product SKU old price in the store. This field must be filled using the period (.) As the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Availability*
     - status
     - String
     - Informs the availability (status) of the product SKU in the store. Possible values: *available* or *unavailable*
   * - Images*
     - images
     - Object Image
     - A map with all the product SKU images. Keys: *weight x high*. Values: image url. The default image should be set into **default** property
   * - Payment conditions
     - installment
     - Object Installment
     - Ingormation about product SKU payment conditions
   * - Cost price
     - base_price
     - Number
     - Informs the cost price, ie the base price of the product SKU in the store. This field must be filled using the period (.) As the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Stock
     - stock
     - Number
     - Quantity of product SKU in stock
   * - Additional Information
     - extra_info
     - Object
     - Map with additional information that does not match any other field of this sku object of the product. Keys: extra name. Values: extra values. Examples: *new*, *promo*, *free_shipping*, *black_friday*, etc.
     
Example:

.. code-block:: json

       "skus": [
        {
            "sku":"203458",
            "specs": {
                "tamanho":"034",
                "cor":"58"
            },
            "status":"available",
            "old_price":499.99,
            "price":379.99,
            "installment": {
                "count":12,
                "price":31.67
            },
            "extra_info": {
                "new":false
            },
            "images":{
                "default":"/180x180/203458.jpg"
            }
        },
        {
            "sku":"203558",
            "specs": {
                "tamanho":"035",
                "cor":"58"
            },
            "status":"available",
            "old_price":499.99,
            "price":379.99,
            "installment": {
                "count":12,
                "price":31.67
            },
            "extra_info": {
                "new":false
            },
            "images": {
                "default":"/180x180/203558.jpg"
            }
        }
        ]
        
- **Kit Product:** When a product is a group of other products, it becomes a kit. A kit contains many products that should be ordered as the value of the **kit_products** property.

Cart
----

The property called **cart** must be inserted in **metadados_recsys**. 

It must be included in the e-commerce **cart** pages. This property contains information about all products inserted in the current cart and should be created as follows:

.. list-table:: Properties of type *cart*
   :widths: auto
   :header-rows: 1
   
   * - Name
     - Key
     - Type
     - Description
   * - Items*
     - items
     - Item Array
     - List of objects of type Item with the products inserted in the cart
   * - Product*
     - product
     - Object Product Cart
     - Detail of the product that is in the cart
   * - Product Id*
     - id
     - String
     - Product id inserted into cart
   * - Product price*
     - price
     - Number
     - Informs the price of the product that is in the cart. This field must be filled using the period (.) As the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Product SKU*
     - sku
     - String
     - SKU code of the product that is in the cart
   * - Item Amount*
     - quantity
     - Number
     - Quantity of product items in the cart
     
Example:

.. code-block:: json

    cart: {
        items: [
        {
            product: {
                id: "882762",
                price: 34.99,
                sku: "882762324996"
            },
            quantity: 1
        },
        {
            product: {
                id: "2222",
                price: 114.99,
                sku: "33333"
            },
            quantity: 2
        }
        ]
    }


Transaction
-----------

The property called **transaction** must be inserted in **metadados_recsys**. 

It must be included in the order **confirmation** pages of an e-commerce . This property contains information about the request that the user made and should be created as follows:

.. list-table:: Properties of type *confirmation*
   :widths: auto
   :header-rows: 1
   
   * - Name
     - Key
     - Type
     - Description
   * - Order Id*
     - id
     - String
     - Unique identifier of the order transaction
   * - Products bought*
     - items
     - Items Array
     - List of purchased cart items. Each item is an object that contains the product, quantity, and tag list
   * - Shipping Information
     - shipping
     - Object Shipping
     - Order shipping information. This object contains cost, delivery date and *tracking*
   * - Service values
     - services
     - Number
     - Services value applied on purchase. Example: guarantee, installation
   * - Signature*
     - signature
     - String
     - Order transaction signature. Generated by combining fields with an MD5 Hash
     
**Signature:**
This step is very important to ensure that the transmitted data is consistent and the receiver is who it claims to be. That's why *signature* property is essential.

To obtain the signature, you must concatenate some transaction properties with the secret key and then generate the MD5 hash. It is then shown step by step to generate the signature of the last example.

.. code-block:: java
    
    //Obs: The fields must be concatenated in this format:
    "orderId:secretId:item1Id,item1Sku,item1Price,item1Quantity:item2Id,item2Sku,item2Price,item2Quantity:..."

Example:

.. code-block:: json

    "transaction": {
        "id":"156144",
        "items": [
        {
            "product": {
                "id":"2",
                "price":169.99,
                "sku":"203458"
            },
            "quantity":1
        }
        ],
        "signature":"bbd04e1a8479a12a8f076a578ec9aecb"
    }
