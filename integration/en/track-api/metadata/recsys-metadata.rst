﻿Metadata
========

The application will receive the e-commerce information through the global variable **metadados_recsys**. This information allows us to track and analyze all user interactions on the application web pages. As a consequence, the following steps must be implemented on every page on the website.

More specifically, the global variable **metadados_recsys** must be defined with the respective information as explained in this document. This variable **must** be positioned inside the *head tag* and above the *tag script*, which calls **recsys.js**.

The next sessions explain each property and all the fields contained in the variable. **Each field with * is required**.

