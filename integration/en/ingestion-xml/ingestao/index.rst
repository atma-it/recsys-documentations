﻿Past Data Injection
===================

The Past Data Injection ia a technique applied to a new Biggy integrated store. This process is intended to integrate the data passed prior to integration with Biggy. By this way, it will be possible to use information that could not be captured by the integration because they had already happened. The result of this is the enrichment of the database and improvement in the performance of the recommendation.

The injection is done in three steps:

1. Users
2. Products
3. Orders

All steps of data injection work in the same way: a XML file is required, obeying a particular order of the fields. Each file must be entered in the Biggy FTP address:

.. code-block:: http
    
    ftp://bp-ftp.centralus.cloudapp.azure.com/files.

The access credentials (username and password) to the FTP address will be made available by Biggy.


Users
-----

A XML file is required for user ingestion, which can be available through a FTP server. The file (UTF-8 encoded) must be called **users.xml** and contains the root element 'users'. 
Each user needs to be declared in a "user" child element with its fields. **All fields with * are required.**


.. list-table:: User properties
   :widths: auto
   :header-rows: 1
   
   * - Field name
     - Description
   * - ID*
     - User Code
   * - Email*
     - User email
   * - Name*
     - User name
   * - Gender*
     - User Gender. This field must be filled only by M (male) or F (female)
   * - Birthday
     - User's date of birth. This field must be completed obeying the following formed: yyyy-MM-dd (example: 2017-03-19)

The following is an example of the XML structure for user ingestion.

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <users>
        <user>
            <id>1</id>
            <email>joao@biggy.com</email>
            <name>Joao Kante</name>
            <gender>M</gender>
            <birthday>1990-01-01</birthday>
        </user>
        <user>
            <id>1</id>
            <email>joze@biggy.com</email>
            <name>Joze Willian</name>
            <gender>M</gender>
            <birthday>1980-01-01</birthday>
        </user>
    </users>


Products
--------

A XML file is required for product ingestion, which can be available through a FTP server. The file (UTF-8 encoded) must be called **products.xml** and contains the root element 'products'. 

Each product specification (SKU) needs to be declared in a "product" child element with its fields. **All fields with * are required.**

.. list-table:: Product properties
   :widths: auto
   :header-rows: 1

   * - Field name
     - Description
   * - ProductId*
     - Product code (root element code - parent code)
   * - Sku*
     - Product specification code (SKU code - child element - child code)
   * - Name*
     - Product or SKU name
   * - Url*
     - Product SKU Detail Url
   * - ImageUrl*
     - SKU image url
   * - FullDescription
     - Full description of the product or SKU
   * - Brand
     - Product brand
   * - PublishedAt
     - SKU publication date on store website. This field must be completed obeying the following format: yyyy-MM-dd (example: 2017-03-19)
   * - CategoryIdA*
     - SKU first level category code
   * - CategoryNameA*
     - SKU first level category name
   * - CategoryIdB
     - SKU second level category code
   * - CategoryNameB
     - SKU second level category name
   * - CategoryIdC
     - SKU third level category code
   * - CategoryNameC
     - SKU third level category name
   * - Status*
     - SKU availability status. This field must be filled only by available (in case of available/active) or unavailable (in case of unavailable/inactive)
   * - OldPrice
     - Old SKU price. This field must be filled using dot (.) as the decimal separator, without the thousands separator, and should not display the currency marker ($)
   * - Price*
     - Product SKU price. This field must be filled using dot (.) as the decimal separator, without the thousands separator, and should not display the currency marker ($)
     
     
The following is an example of the XML structure for product ingestion.

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <products>
        <product>
            <productId>3</productId>
            <sku>3-123</sku>
            <name>Product name 3</name>
            <url>www.biggy.com.br/3-123</url>
            <imageUrl>www.biggy.com.br/images/3-123</imageUrl>
            <fullDescription>Full description of product 3</fullDescription>
            <brand>Brand X</brand>
            <publishedAt>2017-04-25</publishedAt>
            <categoryIdA>1</categoryIdA>
            <categoryIdNameA>Category X</categoryIdNameA>
            <categoryIdB>2</categoryIdB>
            <categoryIdNameB>Category Y</categoryIdNameB>
            <categoryIdC>3</categoryIdC>
            <categoryIdNameC>Category Z</categoryIdNameC>
            <status>available</status>
            <oldPrice>120.45</oldPrice>
            <price>100.52</price>
        </product>
        <product>
            <productId>3</productId>
            <sku>3-456</sku>
            <name>Product name 3</name>
            <url>www.biggy.com.br/3-123</url>
            <imageUrl>www.biggy.com.br/images/3-456</imageUrl>
            <fullDescription>Full description of product 3</fullDescription>
            <brand>Brand X</brand>
            <publishedAt>2017-04-26</publishedAt>
            <categoryIdA>1</categoryIdA>
            <categoryIdNameA>Category X</categoryIdNameA>
            <categoryIdB>2</categoryIdB>
            <categoryIdNameB>Category Y</categoryIdNameB>
            <categoryIdC>3</categoryIdC>
            <categoryIdNameC>Category Z</categoryIdNameC>
            <status>available</status>
            <oldPrice>90.56</oldPrice>
            <price>90.14</price>
        </product>
    </products>
    

Orders
------

A XML file is required for order ingestion, which can be available through a FTP server. The file (UTF-8 encoded) must be called **orders.xml** and contains the root element 'orders'.
Each order needs to be declared in a "order" element. Each item purchased in the order needs to be declared in a 'items' child element with its fields. **All fields with * are required.**

.. list-table:: Order properties
   :widths: auto
   :header-rows: 1

   * - Field name
     - Description
   * - Id*
     - Order number
   * - UserId*
     - User code who purchased the order
   * - Items*
     - Specifications of items in the order (item array)
   * - ProductId*
     - Product code acquired in the order (field of the child object of the item array)
   * - Sku*
     - Product SKU code purchased on order (field of an object inside item array)
   * - UnitPrice*
     - Unit price of the product SKU purchased on request. This field must be filled using dot (.) as the decimal separator, without the thousands separator, and should not display the currency marker ($) (field of an object inside item array)
   * - Quantity*
     - Quantity of product SKUs that were purchased on request (field of an object inside item array)
   * - OrderTotal
     - Total order price. This field must be filled using dot (.) as the decimal separator, without the thousands separator, and should not display the currency marker ($) 
   * - OrderDate*
     - Order closing date. This field must be completed obeying the following format: yyyy-MM-dd (example: 2017-03-19)

The following is an example of the XML structure for order ingestion.

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <orders>
        <order>
            <id>1</id>
            <userId>1</id>
            <items>
                <item>
                    <productId>3</productId>
                    <sku>3-123</sku>
                    <unitPrice>100.90</unitPrice>
                    <quantity>2</quantity>pra
                </item>
                <item>
                    <productId>3</productId>
                    <sku>3-456</sku>
                    <unitPrice>200.30</unitPrice>
                    <quantity>1</quantity>
                </item>
            </items>
            <orderTotal>301.20</orderTotal>
            <orderDate>2017-03-19</orderDate>
        </order>
    </orders>
