﻿Introduction
************

In this section, the purpose is to provide all the information necessary to ingest past data from a store for integration into the Biggy system.
