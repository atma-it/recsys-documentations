﻿Recommendation API for Android
==============================

The Biggy SDK provides customized recommendations with a variety of **strategies** that use **parameters** to process and return a list of **recommendation items**.

For more details on the return possibilities, access the section: :ref:`Recommendation Characteristics <api-ondemand-features-page>`

The calls to the Track and Recommendation APIs are made from the **BiggyClient** object, which is responsible for mounting the request body and parsing the returned return. To instantiate it, simply use the environment variables defined in the **build.gradle** and **apllicationContext** of your application:

.. code-block:: java

    final String apiHost = BuildConfig.BIGGY_API_HOST;
    final String apiKey = BuildConfig.BIGGY_API_KEY
    
    biggyClient = new BiggyClient(getApplicationContext(), apiHost, apiKey);


.. warning:: If this instance is called in several classes, you must instantiate the BiggyClient object once and reuse it throughout your application.

This API consists of three main methods described below:
    - Recommendation Request
    - Recommendation View
    - Recommendation Click

    
Recommendation Request
----------------------

The **RecommendationMetadata** object is responsible for encapsulate all data that will be submitted to the recommendation API and is mandatory on all calls. Following is its structure and a brief description of each field:

.. code-block:: java

    public class RecommendationMetadata {
        Strategy strategy;
        OriginType origin;
        String user;
        List<String> products;
        List<String> categories;
        Integer minProducts;
        Integer maxProducts;
        Boolean mergeRecommendationList;
        Boolean mixMergedRecommendations;
    
        // getters e setters
    }

    
For more details about the properties (parameters), access the section :ref:`Recommendation Request Properties <api-ondemand-request-properties-page>`

The **recommendation** method of the **BiggyClient** class is responsible for performing the API call. This method receives a **RecommendationMetadata** as a parameter and returns a *list of Recommendation*, object that has the following structure:

.. code-block:: java

    public class Recommendation {
        List<String> baseIds;
        List<ProductRecommendation> basesItems;
        List<String> recommendationIds;
        List<ProductRecommendation> recommendationItems;
        
        // getters e setters
    }
    
    public class ProductRecommendation {
        String productId;
        Double score;
        List<ProductRecommendationOffer> offers;
        Set<ProductSpec.Spec> specs;
        
        // getters e setters
    }

    public class ProductRecommendationOffer {
        String offerId;
        String originalProductId;
        String sku;
        String distributionCenter;
        String name;
        String url;
        String imageUrl;
        String secondaryImageUrl;
        Double price;
        Double oldPrice;
        String currencySymbol;
        Map<String, Double> installment;
        boolean hasDiscount;
        float discountPercentage;
        Map<String, String> extraInfo;
        Double score;
        Set<ProductSpec.Spec> specs;
        List<ProductRecommendationCategory> categories;
        
        // getters e setters
    }

    public class ProductSpec extends Entity {
        public static class Spec  {
            public enum TYPE {
                COLOR(new String[]{"cor", "color"}),
                SIZE(new String[]{"tamanho", "size"}),
                FLAVOR(new String[]{"sabor", "flavor"});
            }
        }
        
        private String id;
        private String label;
        private TYPE type;
        private String offerId;
        private Map<String, String> images;
        private Map<String, String> imagesSsl;
        private List<Spec> subSpecs;
        
        // getters e setters
    }
    
    public class ProductRecommendationCategory extends BaseEntity {
        private String storeId;
        private String name;
        private String parent;
        private String originalId;
        private List<String> ancestors;
        
        // getters e setters
    }
    
By this way, the return of the Recommendation is composed of 4 lists:

    1. baseIds
    2. baseItems
    3. recommendationIds
    4. recommendationItems
    
For more details about the return, access the section :ref:`Recommendation Request Return <api-ondemand-request-return-page>`

The following is an example of the recommendation:

.. code-block:: java

    biggyClient.recommendation(recommendationMetadata, new BiggyRecommendationListener() {
        @Override
        public void onRecommendation(List<Recommendation> recommendations) {
            if (recommendations != null) {
                // The Recommendation list has all the recommendations
            } 
        }
    });

With the list of bases and recommendation items it is possible to build the store fronts according to the need of your app, using all the recommendations and personalization strategies offered by Biggy.


Recommendation View
-------------------

The **recommendationView** method in *BiggyClient* enables the Biggy Dashboard to evaluate the performance of the requested recommendation against its visualization. The goal is to call this method when the user "sees" the recommendation. It is worth mentioning that this method performs the call in the Biggy API asynchronously, not affecting the performance of the application.

Following is an example of the recommendationView call:

.. code-block:: java

    biggyClient.recommendationView(recommendationViewMetadata);
    
The **RecommendationViewMetadata** object is responsible for encapsulate all data that will be submitted in the *recommendationView* call. Fields entered are required for all calls. Following is its structure and a brief description of each field:

.. code-block:: java

    public class RecommendationViewMetadata {
        Strategy strategy;
        OriginType origin;
        String session;
        
        // getters e setters
    }

.. note:: The *strategy* and *origin* attributes follow the same existing structure for the Recommendation Request. For more details, access the sections :ref:`Strategies <api-strategy-page>` and :ref:`Origin <api-origin-page>`

Recommendation Click
--------------------

The **recommendationClick** method in *BiggyClient* allows you to evaluate through the Biggy Dashboard the performance of the requested recommendation in relation to the triggering of click events. The purpose is to call this method when the user "clicks" on some product of the recommendation. It is worth mentioning that this method performs the call in the Biggy API asynchronously, not affecting the performance of the application.

Following is an example of the recommendationClick call:

.. code-block:: java
    
    biggyClient.recommendationClick(recommendationClickMetadata);

The **RecommendationClickMetadata** object is responsible for encapsulate all data that will be submitted in the *recommendationClick* call. Fields entered are required for all calls. Following there is a structure and a brief description of each field:

.. code-block:: java

    public class RecommendationClickMetadata {
        Strategy strategy;
        OriginType origin;
        String session;
        String productId;
        
        // getters e setters
    }

.. note:: The *strategy* and *origin* attributes follow the same existing structure for the Recommendation Request. For more details, access the sections :ref:`Strategies <api-strategy-page>` and :ref:`Origin <api-origin-page>`

.. note:: The attribute **productId** is a *String* and should inform which product ID the user clicked on the recommendation. **This is a required attribute.**



.. note:: If you prefer to make direct recommendation calls in another way, you can request the recommendations by calling the Biggy Recommendation API directly from your application, returning in JSON format, however **it is mandatory** to perform of the three methods (Recommendation Request, Recommendation View) if you prefer this option. Note that performing the call in this manner **does not replace** also the implementation of the track integration. For more information, consult the Biggy team.
