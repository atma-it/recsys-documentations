﻿First steps
===========

In order to facilitate the integration process with Android applications, Biggy offers a **SDK** (Software Development Kit) that abstracts calls in our Recommendation and Tracking APIs. With this SDK you can use all available resources to track user navigation, as well as request recommendation in different contexts.

SDK Types
---------

Biggy offers two types of SDK:

1. Compact version (*biggysdk.jar*)

2. Version with dependencies (*biggysdk-with-dependencies.jar*)

.. note:: Regardless of the type selected, inclusion in the Android app project takes place in the same way.

Compact version
---------------

The compact version of the SDK contains only the classes and calls developed by Biggy. It is a version with a smaller size, however for its operation, it is imperative to add the necessary dependencies in the **build.gradle** of your Android application. It is an option for those who want to keep track of version versions of the application-side dependencies or for those who already have these dependencies in their *build.gradle*, so it avoids version mismatch conflicts. Are they:
- Gson
- Google Inject
- OkHttp

Dependencies:

.. code-block:: gradle

    //GSON:
    compile 'com.google.code.gson:gson:2.7'
    //Google Inject:
    compile group: 'com.google.inject', name: 'guice', version: '4.0'
    //OkHttp:
    compile 'com.squareup.okhttp:okhttp:2.2.0'


Version with dependencies
-------------------------

The version with dependency of the SDK already has internally the dependencies previously mentioned in the *.jar*. It is a larger version and is indicated for those who do not want to change or add other dependencies in the **build.gradle** of your Android application.


Include the SDK in an Android application
-----------------------------------------

The Biggy SDK is a .jar extension library that must be included in the Android application dependencies.
Here's a simple step-by-step for adding dependency using the Android Studio IDE:

1. Copy the .jar to "<app name>/app/libs/".
2. Open the "Project Sctructure -> Dependencies" option, click on the addition of a "Jar Dependency".

Setting Up Biggy Keys
---------------------

For the operation of the integration it is necessary to add in the **build.gradle** some environment variables that are used in the requests for the Tracking and Recommendation APIs.

.. image:: add-dependency-01.png

.. image:: add-dependency-02.png


These variables will be delivered by Biggy and should be added as follows in the **build.gradle**:

.. code-block:: json

    buildTypes {
        debug {
            buildConfigField "String", "BIGGY_API_HOST", "<biggy_api_host_value>"
            buildConfigField "String", "BIGGY_API_KEY", "<biggy_api_key_value>"
            buildConfigField "String", "BIGGY_SECRET_KEY", "<biggy_secret_key_value>"
        }
        release {
            buildConfigField "String", "BIGGY_API_HOST", "<biggy_api_host_value>"
            buildConfigField "String", "BIGGY_API_KEY", "<biggy_api_key_value>"
            buildConfigField "String", "BIGGY_SECRET_KEY", "<biggy_secret_key_value>"
        }
    }

To obtain the values, simply make the call using the class **BuildConfig**:

.. code-block:: java

    final String apiHost = BuildConfig.BIGGY_API_HOST;
    final String apiKey = BuildConfig.BIGGY_API_KEY;
