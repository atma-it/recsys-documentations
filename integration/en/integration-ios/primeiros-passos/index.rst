﻿First steps
===========

In order to facilitate the integration process with iOS applications, Biggy provides a **Framework** that abstracts our Recommendation and Tracking APIs calls.

With this framework it is possible to use all available resources to track user navigation, as well as request recommendations in various contexts. The framework was written in Swift 4, but is compatible with Swift 3, with Objective-C and works in versions later than iOS 8.


Include Framework in iOS applications
-------------------------------------

The ".framework" file should be copied to the root folder of the application and be referenced in the Embedded Binaries and Linked Frameworks and Libraries fields in the project settings.

To import the framework classes into a project file:

.. code-block:: json

    Swift:
        import RecsysIOS
    
    
.. code-block:: json
        
    Objective-C:
        #import <RecsysIOS/RecsysIOS-Swift.h>


Setting Up Biggy Properties
---------------------------

For the integration operation it is necessary to add a "Property List" file with some environment variables that are used in the requests for the Tracking and Recommendation APIs.

These variables will be delivered by Biggy and should be added as follows:


.. image:: add-property-01.png

