﻿Recommendation API for IOS
==========================

A Biggy Framework provides customized recommendations with a variety of **strategies** that use **parameters** to process and return a list of **recommendation items**.

For more details on the return possibilities, access the section: :ref:`Recommendation Characteristics <api-ondemand-features-page>`

The calls to the Track and Recommendation APIs are made from the **BGYBiggyClient** object, which is responsible for mounting the request body and parsing the returned return. 

To instantiate a shared version of the object, just pass an object of type [String: AnyObject] instantiated from a **plist** file containing the properties presented in the previous item, or pass the path to the file, as shown:

.. code-block:: swift

    if let path = Bundle.main.path(forResource: "Biggy", ofType: "plist") {
        try? BGYBiggyClient.instantiate(contentsOfFile: path)
    }

You can use the **shared** property to use the object. Note that the **instantiate** function must have been called before, since the shared object is of type **"BGYBiggyClient?"**.

This API consists of three main methods described below:
    - Recommendation Request
    - Recommendation View
    - Recommendation Click

Recommendation Request
----------------------

The **BGYRecommendationMetadata** structure is responsible for encapsulate all data that will be submitted to the recommendation API and is mandatory on all calls. Following is its structure and a brief description of each field:

.. code-block:: objectivec

    @objc public class BGYRecommendationMetadata: NSObject, Mappable {
    
        // Required
        public var strategy: BGYStrategy!
        var sessionId: String? // required but automatically set by BiggyClient
        
        // Optional
        public var origin: BGYOrigin?
        public var user: String?
        public var products: [String]?
        public var categories: [String]?
        public var anonymousUser: String?
        
        public init(strategy: BGYStrategy) {
            self.strategy = strategy
        }
    }
    
    @objc public enum BGYStrategy: Int, RawRepresentable {
        case offers_store
        case offers_category
        case offers_user
        case most_viewed_store
        case most_viewed_category
        case most_viewed_campaign
        case most_viewed_user
        case best_sellers_store
        case best_sellers_category
        case best_sellers_campaign
        case best_sellers_user
        case new_releases_store
        case new_releases_category
        case new_releases_user
        case click_history
        case navigation_history
        case order_history
        case cart_abandonment
        case bought_together
        case best_choice
        case similar_products
    }
    
    @objc public enum BGYOrigin: Int, RawRepresentable {
        case home
        case product
        case product_unavailable
        case category
        case campaign
        case cart
        case empty_cart
        case checkout
        case confirmation
        case search
        case empty_search
        case not_found
        case order_list
        case order_detail
        case wishlist
        case internals
    }

For more details about the properties (parameters), access the section :ref:`Recommendation Request Properties <api-ondemand-request-properties-page>`

The **recommendation** method of the **BGYBiggyClient** class is responsible for performing the API call. This method receives a **BGYRecommendationMetadata** and the funtion as parameters and the function *completion* is called with a *BGYrecommendation* array as parameter, object that has the following structure:

.. code-block:: objectivec

    @objc public class BGYRecommendation: NSObject, Mappable {   
        public var baseIds: [String]?
        public var baseItems: [BGYProductRecommendation]?
        public var recommendationItems: [BGYProductRecommendation]?
        public var recommendationIds: [String]?
    }

By this way, the return of the Recommendation is composed of 4 lists:

    1. baseIds
    2. baseItems
    3. recommendationIds
    4. recommendationItems
    
For more details about the return, access the section :ref:`Recommendation Request Return <api-ondemand-request-return-page>`

With the list of bases and recommendation items it is possible to build the store fronts according to the need of your app, using all the recommendations and personalization strategies offered by Biggy.

The following is an example of the recommendation:

.. code-block:: objectivec

    try!  BGYBiggyClient.shared?.recommendation(recommendationMetadata:  recMetadata!, 
    completion: { recommendations in
        // recommendations is an BGYRecommendation list object
    })


Recommendation View
-------------------

The **recommendationView** method in *BGYBiggyClient* enables the Biggy Dashboard to evaluate the performance of the requested recommendation against its visualization. The goal is to call this method when the user "sees" the recommendation. It is worth mentioning that this method performs the call in the Biggy API asynchronously, not affecting the performance of the application.

Following is an example of the recommendationView call:

.. code-block:: objectivec

    try! BGYBiggyClient.shared?.recommendationView(recommendationViewMetadata: recView!)
    
The **BGYRecommendationViewMetadata** object is responsible for encapsulate all data that will be submitted in the *recommendationView* call. Fields entered are required for all calls. Following is its structure and a brief description of each field:

.. code-block:: objectivec

    @objc public class BGYRecommendationViewMetadata: NSObject, Mappable {
        // Required
        public var strategy: BGYStrategy!
        
        // Optional
        public var origin: BGYOrigin?
    }

.. note:: The *strategy* and *origin* attributes follow the same existing structure for the Recommendation Request. For more details, access the sections :ref:`Strategies <api-strategy-page>` and :ref:`Origin <api-origin-page>`


Recommendation Click
--------------------

The **recommendationClick** method in *BGYBiggyClient* allows you to evaluate through the Biggy Dashboard the performance of the requested recommendation in relation to the triggering of click events. The purpose is to call this method when the user "clicks" on some product of the recommendation. It is worth mentioning that this method performs the call in the Biggy API asynchronously, not affecting the performance of the application.

Following is an example of the recommendationClick call:

.. code-block:: objectivec
    
    try! BGYBiggyClient.shared?.recommendationClick(recommendationClickMetadata: recClick!)

The **BGYRecommendationClickMetadata** object is responsible for encapsulate all data that will be submitted in the *recommendationClick* call. Fields entered are required for all calls. Following there is a structure and a brief description of each field:

.. code-block:: objectivec

    @objc public class BGYRecommendationClickMetadata: NSObject, Mappable {
        // Required
        public var strategy: BGYStrategy!
        public var productId: String!
        
        // Optional
        public var origin: BGYOrigin? 
    }


.. note:: The *strategy* and *origin* attributes follow the same existing structure for the Recommendation Request. For more details, access the sections :ref:`Strategies <api-strategy-page>` and :ref:`Origin <api-origin-page>`

.. note:: The attribute **productId** is a *String* and should inform which product ID the user clicked on the recommendation. **This is a required attribute.**



.. note:: If you prefer to make direct recommendation calls in another way, you can request the recommendations by calling the Biggy Recommendation API directly from your application, returning in JSON format, however **it is mandatory** to perform of the three methods (Recommendation Request, Recommendation View) if you prefer this option. Note that performing the call in this manner **does not replace** also the implementation of the track integration. For more information, consult the Biggy team.
