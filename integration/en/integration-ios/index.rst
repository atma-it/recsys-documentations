﻿Introduction
************

In this section, the purpose is to provide all the necessary information about the system architecture, data flow and the steps required to integrate and use the services provided by Biggy on an IOS system.
