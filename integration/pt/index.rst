﻿INTEGRAÇÃO COM O SISTEMA DE RECOMENDAÇÃO BIGGY
**********************************************

Bem-vindo ao manual de integração com o Sistema de Recomendação da Biggy!

O documento está organizado em seções que segmentam os serviços fornecidos pela Biggy:

.. _overview-docs:

.. toctree::
   :maxdepth: 3   
   :caption: Visão Geral
   
   overview/index.rst   
  
.. _integration-docs:

.. toctree::
   :maxdepth: 3   
   :caption: Integração
   
   track-api/index.rst
   track-api/primeiros-passos/index.rst
   track-api/layout/index.rst
   track-api/metadata/index.rst
   track-api/metadata/recsys-script.rst
   track-api/metadata/recsys-metadata.rst
   track-api/metadata/recsys-metadata-properties.rst
   track-api/others/site-integration.rst
   track-api/others/front-integration.rst
   track-api/others/past-data-injection.rst

.. _ingestion-xml-docs:

.. toctree::
   :maxdepth: 3   
   :caption: Ingestão de XML
   
   ingestion-xml/index.rst
   ingestion-xml/ingestao/index.rst

.. _recommendation-api-docs:

.. toctree::
   :maxdepth: 3   
   :caption: API de Recomendação
   
   recommendation-api/index.rst
   recommendation-api/b-api/index.rst
   recommendation-api/b-api/campaign.rst
   recommendation-api/b-api/ondemand.rst

.. _integration-android-docs:

.. toctree::
   :maxdepth: 3   
   :caption: Integração Android
   
   integration-android/index.rst
   integration-android/visao-geral/index.rst
   integration-android/primeiros-passos/index.rst
   integration-android/api-recomendacao/index.rst

.. _integration-ios-docs:

.. toctree::
   :maxdepth: 3   
   :caption: Integração IOS        
   
   integration-ios/index.rst
   integration-ios/visao-geral/index.rst
   integration-ios/primeiros-passos/index.rst
   integration-ios/api-recomendacao/index.rst

.. _recommendation-properties-docs:

.. toctree::
   :maxdepth: 4   
   :caption: Especificações da Recomendação 

   shared/recommendation-api-ondemand/api-features.rst
   shared/recommendation-api-ondemand/request-properties.rst
   shared/recommendation-api-ondemand/api-strategy.rst
   shared/recommendation-api-ondemand/api-origin.rst
   shared/recommendation-api-ondemand/request-return.rst



Índices e tabelas
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
