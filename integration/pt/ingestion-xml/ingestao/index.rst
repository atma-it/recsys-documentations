﻿Injeção de dados passados
=========================

A injeção de dados passados é uma técnica aplicada a uma nova loja integrada na Biggy. Este processo destina-se a ingerir os dados passados ​​antes da integração com a Biggy. Desta forma, será possível usar informações que não puderam ser capturadas pela integração porque já haviam acontecido. O resultado disso é o enriquecimento da base de dados e melhoria no desempenho da recomendação.

A injeção é feita em três etapas:

1. Usuários
2. Produtos
3. Pedidos

Todas as etapas do trabalho de injeção de dados da mesma maneira: é necessário um arquivo XML, obedecendo a um ordem particular dos campos. Cada arquivo deve ser inserido no endereço Biggy FTP:

.. code-block:: http
    
    ftp://bp-ftp.centralus.cloudapp.azure.com/files.

As credenciais de acesso (nome de usuário e senha) para o endereço FTP serão disponibilizados pela Biggy.


Usuários
--------

É necessário um arquivo XML para a ingestão do usuário, que pode estar disponível através de um FTP servidor. O arquivo (UTF-8 codificado) deve ser chamado **users.xml** e contém a raiz elemento 'users'.
Cada usuário precisa ser declarado em um elemento filho "user" com seus campos. **Todos os campos com * são obrigatórios.**

.. list-table:: Propriedades do usuário
   :widths: auto
   :header-rows: 1

   * - Nome do Campo
     - Descrição
   * - ID*     
     - Código do usuário
   * - Email*     
     - E-mail do usuário
   * - Name* 
     - Nome do usuário
   * - Gender*
     - Gênero do usuário. Este campo deve ser preenchido apenas por M (masculino) ou F (feminino)
   * - Birthday
     - Data de nascimento do usuário. Este campo deve ser completado obedecendo o seguinte formado: aaaa-MM-dd (exemplo: 2017-03-19)    


A seguir um exemplo com a estrutura do XML para a ingestão de usuários.

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <users>
        <user>
            <id>1</id>
            <email>joao@biggy.com</email>
            <name>Joao Kante</name>
            <gender>M</gender>
            <birthday>1990-01-01</birthday>
        </user>
        <user>
            <id>1</id>
            <email>joze@biggy.com</email>
            <name>Joze Willian</name>
            <gender>M</gender>
            <birthday>1980-01-01</birthday>
        </user>
    </users>


Produtos
--------

É necessário um arquivo XML para a ingestão do produto, que pode estar disponível através de um FTP servidor. O arquivo (codificado em UTF-8) deve ser chamado **products.xml** e contém o elemento raiz 'products'. Cada especificação de produto (SKU) precisa ser declarada em um elemento filho "product" com seus campos. **Todos os campos com * são obrigatórios.**

.. list-table:: Propriedades do produto
   :widths: auto
   :header-rows: 1

   * - Nome do Campo
     - Descrição
   * - ProductId*     
     - Código do produto (código do elemento raiz - parent code)
   * - Sku*     
     - Código da especificação do produto (código da SKU - elemento filho - child code)
   * - Name* 
     - Nome do produto ou da SKU
   * - Url*
     - Url do detalhe da SKU do produto 
   * - ImageUrl*
     - Url da imagem da SKU    
   * - FullDescription
     - Descrição completa do produto ou da SKU
   * - Brand
     - Marca do produto
   * - PublishedAt
     - Data de publicação do SKU no site da loja. Este campo deve ser preenchido obedecendo ao seguinte formato: aaaa-MM-dd (exemplo: 2017-03-19)
   * - CategoryIdA*
     - Código da categoria de primeiro nível da SKU
   * - CategoryNameA*
     - Nome da categoria de primeiro nível da SKU
   * - CategoryIdB
     - Código da categoria de segundo nível da SKU
   * - CategoryNameB
     - Nome da categoria de segundo nível da SKU
   * - CategoryIdC
     - Código da categoria de terceiro nível da SKU
   * - CategoryNameC
     - Nome da categoria de terceiro nível da SKU
   * - Status*
     - Status de disponibilidade do SKU. Este campo deve ser preenchido apenas por disponível (no caso de disponível/ativo) ou indisponível (em caso de indisponível/inativo)
   * - OldPrice
     - Preço antigo da SKU do produto. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - Price*
     - Preço da SKU do produto. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)


A seguir um exemplo com a estrutura do XML para a ingestão de produtos.

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <products>
        <product>
            <productId>3</productId>
            <sku>3-123</sku>
            <name>Product name 3</name>
            <url>www.biggy.com.br/3-123</url>
            <imageUrl>www.biggy.com.br/images/3-123</imageUrl>
            <fullDescription>Full description of product 3</fullDescription>
            <brand>Brand X</brand>
            <publishedAt>2017-04-25</publishedAt>
            <categoryIdA>1</categoryIdA>
            <categoryIdNameA>Category X</categoryIdNameA>
            <categoryIdB>2</categoryIdB>
            <categoryIdNameB>Category Y</categoryIdNameB>
            <categoryIdC>3</categoryIdC>
            <categoryIdNameC>Category Z</categoryIdNameC>
            <status>available</status>
            <oldPrice>120.45</oldPrice>
            <price>100.52</price>
        </product>
        <product>
            <productId>3</productId>
            <sku>3-456</sku>
            <name>Product name 3</name>
            <url>www.biggy.com.br/3-123</url>
            <imageUrl>www.biggy.com.br/images/3-456</imageUrl>
            <fullDescription>Full description of product 3</fullDescription>
            <brand>Brand X</brand>
            <publishedAt>2017-04-26</publishedAt>
            <categoryIdA>1</categoryIdA>
            <categoryIdNameA>Category X</categoryIdNameA>
            <categoryIdB>2</categoryIdB>
            <categoryIdNameB>Category Y</categoryIdNameB>
            <categoryIdC>3</categoryIdC>
            <categoryIdNameC>Category Z</categoryIdNameC>
            <status>available</status>
            <oldPrice>90.56</oldPrice>
            <price>90.14</price>
        </product>
    </products>
    

Pedidos
-------

É necessário um arquivo XML para a ingestão de pedidos, que pode estar disponível através de um FTP servidor. O arquivo (UTF-8 codificado) deve ser chamado **orders.xml** e contém a raiz elemento 'orders'.
Cada pedido precisa ser declarado em um elemento filho "order" com seus campos. Cada item comprado no depido precisa ser declarado em um elemento filho "items" com seus campos. **Todos os campos com * são obrigatórios.**

.. list-table:: Propriedades do pedido
   :widths: auto
   :header-rows: 1

   * - Nome do Campo
     - Descrição
   * - Id*     
     - Código do pedido
   * - UserId*     
     - Código do usuário que efetuou o pedido
   * - Items* 
     - Especificações dos items presentes no pedido (array de item)
   * - ProductId*
     - Código do produto adquirido no pedido (campo do objeto filho do array de item)
   * - Sku*
     - Código da SKU do produto adquirido no pedido (campo do objeto filho do array de item)
   * - UnitPrice*
     - Preço unitário da SKU do produto adquirido no pedido. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($) (campo do objeto filho do array de item)
   * - Quantity*
     - Quantidade de SKUs do produto adquirido na compra (campo do objeto filho do array de item)
   * - OrderTotal
     - Preço do total do pedido. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - OrderDate*
     - Data do fechamento do pedido. Este campo deve ser preenchido obedecendo ao seguinte formato: aaaa-MM-dd (exemplo: 2017-03-19)

A seguir um exemplo com a estrutura do XML para a ingestão de pedidos.

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <orders>
        <order>
            <id>1</id>
            <userId>1</id>
            <items>
                <item>
                    <productId>3</productId>
                    <sku>3-123</sku>
                    <unitPrice>100.90</unitPrice>
                    <quantity>2</quantity>
                </item>
                <item>
                    <productId>3</productId>
                    <sku>3-456</sku>
                    <unitPrice>200.30</unitPrice>
                    <quantity>1</quantity>
                </item>
            </items>
            <orderTotal>301.20</orderTotal>
            <orderDate>2017-03-19</orderDate>
        </order>
    </orders>
