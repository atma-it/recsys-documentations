﻿Recsys Script
=============

Todas as páginas web do comércio eletrônico devem importar o script do Biggy responsável por encapsular as chamadas a nossa API de tracking.

Existem duas maneiras de realizar a importação do script e consequentemente a chamada a nossa API:

- **Automática:** utilizada somente se no momento da importação do script existe a garantia de que o metadado já está criado e não sofrerá alteração. Nesse cenário, basta importar o script recsys.js, que realizará a chamada a API automáticamente no final do script.

- **Manual:** utilizada quando é necessário um controle maior nas chamadas a API. Nesse caso, o script prepara o object JavaScript RecSys que será utilizado pelo seu site nos momentos apropriados. Nesse cenário, basta importar o script **recsys.js** e realizar chamadas utilizando o método RecSys.Tracker passando como parâmetro os atributos: _recsysa.id, _recsysa.host, _recsysa.options. Esses atributos serão exemplificados mais adiante.

.. note:: Para garantir a integração completa entre o comércio eletrônico e a solução de Biggy, a importação do script deve ocorrer em todas das páginas da aplicação web utilizando o código abaixo:

.. code-block:: javascript

    (function () { 
        var sid = '<ADD_STORE_ID>',
            host = '<ADD_API_HOST>',
            options = {};         
            
        _recsysa = { 
            id: sid, 
            host: host, 
            options: options 
            }; 
        
        metadados_recsys = window.metadados_recsys || {};
        var script = document. createElement('script');
        script.src = '<ADD_SCRIPT_URL>';
        script.async = true; 
        
        var firstScript = document.getElementsByTagName('script') [0];
        firstScript.parentNode.insertBefore(script, firstScript); 
    }()); 


.. note:: Os campos **'<ADD_STORE_ID>'**, **'<ADD_API_HOST>'** e **'<ADD_SCRIPT_URL>'** serão enviados pela equipe da Biggy durante o processo de integração. 
