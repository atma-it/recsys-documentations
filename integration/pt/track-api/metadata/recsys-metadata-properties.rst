﻿Propriedades dos Metadados
==========================

A variável global **metadados_recsys** deve ser implementada nas páginas web do e-commerce. A variável possui algumas propriedades que devem ser informadas dependendo de qual página está sendo o **metadados_recsys** a partir.


Page
----

A propriedade chamada **page** deve ser inserida em **metadados_recsys**. *Deve ser incluída em todas as páginas.*

Esta propriedade contém informações sobre a navegação do usuário e deve ser criada como o exemplo a seguir:

.. list-table:: Propriedades do tipo *page*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - Nome da página*
     - name
     - String
     - Descreve qual página o usuário está navegando no momento. Valores possíveis: home, product, product_unavailable, category, campaign, cart, empty_cart, checkout, confirmation, search, empty_search, not_found, order_list, order_detail, internals, unknown
     
Exemplo:

.. code-block:: json

    page: {
        name: "home"
    }


User
----

A propriedade chamada **user** deve ser inserida em **metadados_recsys**. 

*Deve ser incluída em todas as páginas.*

Se o usuário não está logado, o objeto pode ser omitido. 

Esta propriedade contém informações sobre o usuário que está navegando no site e deve ser criado como segue:


.. list-table:: Propriedades do tipo *user*  
   :widths: auto
   :header-rows: 1

   * - Nome do campo
     - Chave
     - Tipo
     - Descrição    
   * - User Id*
     - id
     - String
     - O código que o site usa internamente para identificar esse usuário
   * - Nome completo*
     - name
     - String
     - O nome completo do usuário
   * - E-mail*
     - email
     - String
     - E-mail do usuário
   * - Allow e-mail*
     - allow_mail_marketing
     - Boolean
     - Informa se o usuário autoriza o recebimento de e-mails da loja. Valores possíveis: *true* ou *false*
   * - Nome de usuário
     - username
     - String
     - Nome usado para identificar o usuário no sistema
   * - Nickname
     - nickname
     - String
     - Nome do tratamento utilizado pelo usuário, como você gostaria de ser chamado.
   * - Data de nascimento
     - birthday
     - String
     - Data de nascimento do usuário. O campo deve obedecer o formarto yyyyMMdd. Exemplo: 19820417
   * - Idioma do usuário
     - language
     - String
     - Preferência de idioma do usuário no formato compatível com IETF. Exemplo: en-US, pt-BR
   * - Tags
     - tags
     - Array de String
     - As tags do usuário são as informações transmitidas de cada usuário de forma linear e sem hierarquia
   * - CEP
     - zipcode
     - String
     - Código postal do usuário (CEP)
   * - País
     - country
     - String
     - País do usuário País seguindo o padrão *two letter ISO 31661 alpha 2*. Exemplo: BR, US, PT, etc.
   * - Gênero
     - gender
     - String
     - Gênero do usuário. Valores possíveis: M ou F
   * - Identificação Nacional
     - document_id
     - String
     - Identificação nacional do usuário. Exemplo: Registro Nacional, Número da Segurança Social, etc.
   * - Informações Adicionais
     - extra_info
     - Object
     - Mapa com informações adicionais que não coincidem com nenhum outro campo deste objeto usuário

Exemplo:

.. code-block:: json

    user: {
        id: "1",
        name: "Jose Rodolfo",
        email: "joserodolfo@email.com",
        language: "pt-BR",
        country: "BR",
        gender: "M",
        allow_mail_marketing: true
    }


Categories
----------

A propriedade chamada **categories** deve ser inserida em **metadados_recsys**. 

Deve ser incluída nas páginas de categoria. Essas páginas representam os departamentos do e-commerce.

.. note:: É importante que os produtos, pertencentes a uma subcategoria, tenham a mesma estrutura de categoria da página que está incluída. 

.. list-table:: Propriedades do tipo *categories*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - Id da categoria*
     - id
     - String
     - Identificador único da categoria
   * - Nome da categoria*
     - name
     - String
     - Nome da categoria
   * - Categorias-pai
     - parents
     - Array de Categories
     - Lista de objetos do tipo Categories que são as categorias-pai da categoria informada
     
Exemplo:

.. code-block:: json

    categories :[
    {
        id: "123",
        name: "Tênis Casual",
        parents" :[
            "321"
        ],
    },
    {
        id: "321",
        name: "Tênis"
    }
    ]


Search
------

A propriedade chamada **search** deve ser inserida em **metadados_recsys**. 

Deve ser incluída nas páginas de **resultados de buscas** do e-commerce e deve ser criada da seguinte maneira:

.. list-table:: Propriedades do tipo *search*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - Termo de busca*
     - query
     - String
     - Termo usado para efetuar a busca
   * - Produtos retornados na busca
     - items
     - Array de Products
     - Lista de objetos do tipo Product, que são os produtos retornados na busca. Nesse caso, só o Id do produto é necessário.
     
Exemplo:

.. code-block:: json

    search: {
        query: "tenis e camiseta",
        items: [
        {
            id: "1"
        },
        {
            id: "2"
        }
        ]
    }


Product
-------

A propriedade chamada **product** deve ser inserida em **metadados_recsys**. 

Esta propriedade contém informações sobre o produto que o usuário está visualizando na página web. 

*Deve ser incluída na página de detalhe do produto.*

Existem diferentes tipos de informações para produtos distintos, dependendo da sua categoria ou de qualquer particularidade. Para organizá-lo, dividimos nossa estrutura em diferentes sessões: 
    - Itens essenciais
    - Especificações (specs)
    - SKUs
    - Kit de produtos (product kit).

Neste arranjo, todas as propriedades estão incluídas apenas na variável **product**.

**Obervação:** Todas as propriedades obrigatórias são marcadas com "*"

- **Itens Essenciais (Essentials):** A tabela a seguir apresenta todas as propriedades essenciais suportadas. Deve-se preencher o máximo de informações possível.

.. list-table:: Propriedades do tipo *product* classificadas como Itens Essenciais (Essentials)
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - Id do produto*
     - id
     - String
     - Identificador único do produto dentro da loja
   * - Nome*
     - name
     - String
     - Nome completo do produto
   * - Url do Produto*
     - url
     - String
     - Endereço completo do produto no e-commerce. Informar sem os protocolos *http* ou *https**
   * - Imagens*
     - images
     - Object Image
     - Um mapa com todas as imagens do produto. Chaves: largura x altura (*weight x high*). Valores: url da imagem. A imagem padrão deve ser definida como **default**
   * - Disponibilidade*
     - status
     - String
     - Informa a disponibilidade (status) do produto na loja. Valores possíveis: *available* ou *unavailable*
   * - Categorias*
     - categories
     - Array de Categories
     - Conjunto das categorias que o produto pertence
   * - Preço*
     - price
     - Number
     - Informa o preço do produto na loja. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - Preço antigo
     - old_price
     - Number
     - Informa o preço antigo do produto na loja. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - Descrição
     - description
     - String
     - Descrição completa do produto. Deve ser um texto sem tags HTML
   * - Tags
     - tags
     - Array de Object
     - Conjunto de tags que o produto contém
   * - Marca
     - brand
     - String
     - Marca do produto
   * - Data de publicação
     - published
     - String
     - Data que o produto foi adicionado no site. O campo deve obedecer o formarto yyyyMMdd. Exemplo: 20160217
   * - Especificações do produto
     - specs
     - Object Spec
     - Declaração de todas as especificações que o produto pode ter. Mais detalhes no tópico "Especificações"
   * - SKUs
     - skus
     - Array de Sku
     - Listagem das SKUs do produto e suas características. Mais detalhes no tópico "Skus"
   * - Unidade de medição
     - unit
     - String
     - Unidade de medição utilizada no produto. Informar essa e isso apenas para produtos que contenham quantidades fracionadas
   * - Kit de produtos
     - kit_products
     - Array de Product
     - Listagem dos produtos pertencentes ao kit
   * - Informações Adicionais
     - extra_info
     - Object
     - Mapa com informações adicionais que não coincidem com nenhum outro campo deste objeto produto. Chaves: nome extra. Valores: valores extra. Exemplos: *new*, *promo*, *free_shipping*, *black_friday*, etc

Exemplo:

.. code-block:: json

    product: {
        id: "2",
        name: "Tênis 2 - Feminino",
        description: "...",
        url": "www.ecommerce.com.br/tenis2feminino", 
        images: {
            default: "/180x180/2.jpg"
        }, 
        status: "available",
        old_price: 499.99,   
        price: 379.99,
        installment: {
            count: 12,
            price: 31.67
        },
        tags: [
        {
            id: "calçados",
            name: "calçados"
        },       
        {
            id: "tênis",
            name: "tênis"
        }
        ],
        "extra_info": {
            "new":false,
            "promo":true,
        },
        specs: {…},
        skus: [...]
    }
    
- **Especificações (specs):** Cada produto pode ter sua própria coleção de recursos com valores dinâmicos, chamadas especificações. Ele deve ser declarado na propriedade **specs** dentro do objeto **product** . A tabela a seguir apresenta todas as propriedades das especificações.

.. list-table:: Propriedades das Especificações (*specs*) de um *product*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - Cor
     - cor
     - String
     - Identificador de possíveis cores do produto (ou de sua SKU)
   * - Tamanho
     - tamanho
     - String
     - Identificador de possíveis tamanhos do produto (ou de sua SKU)
   * - Voltagem
     - voltagem
     - String
     - Identificador de possíveis voltagens do produto (ou de sua SKU)
   * - Tipo da mídia
     - midia
     - String
     - Identificador de possíveis tipos de mídia do produto (ou de sua SKU). Possíveis valores: "papel", "e-book", "DVD", etc
   
Exemplo:

.. code-block:: json

    "specs": {
        "cor": [
        {
            "id":"58",
            "label":"cinza",
            "url":"www.ecommerce.com.br/tenis2feminino?cor=58",
            "images": {
                "default":"/180x180/258.jpg"
            }
        },
        {
            "id":"6S",
            "label":"azul",
            "url":"www.ecommerce.com.br/tenis2feminino?cor=6S",
            "images": {
                "default":"/180x180/26S.jpg"
            }
        }
        ],
        "tamanho": [
            {
                "id":"034",
                "label":"34"
            },
            {
                "id":"035",
                "label":"35"
            }
        ]
        }
        
- **SKUs:** o produto pode ter uma **lista de SKUs** que representam: 
    
    1. Combinações de especificações definidas no produto (combinações de **specs**);
    2. Detalhes do preço, como preço (*price*), preço antigo (*old_price*) e prestação (*installment*) distintos.
    
As propriedades a seguir podem ser declaradas no produto e SKU também:
    - name
    - url
    - description
    - images
    - images_ssl
    - status
    - ean_code

.. note:: No caso de ambos serem declarados, a propriedade da SKU possui uma prioridade maior que propriedade do produto.

A tabela a seguir apresenta todas as propriedades de uma SKU.

.. list-table:: Propriedades da SKU de um *product*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - SKU*
     - sku
     - String
     - Código da SKU que representa a combinação das especificações definidas nesse objeto
   * - Especificações da SKU*
     - specs
     - Objeto Spec
     - Mapa com os identificadores de especificação da SKU
   * - Preço*
     - price
     - Number
     - Informa o preço da SKU do produto na loja. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - Preço antigo
     - old_price
     - Number
     - Informa o preço antigo da SKU do produto na loja. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - Disponibilidade*
     - status
     - String
     - Informa a disponibilidade (status) da SKU do produto na loja. Valores possíveis: *available* ou *unavailable*
   * - Imagens*
     - images
     - Object Image
     - Um mapa com todas as imagens da SKU do produto. Chaves: largura x altura (*weight x high*). Valores: url da imagem. A imagem padrão deve ser definida como **default**
   * - Condições de Pagamento
     - installment
     - Object Installment
     - Identificador das condições de pagamento da SKU do produto
   * - Preço de Custo
     - base_price
     - Number
     - Informa o preço de custo, ou seja, o preço base da SKU do produto na loja. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - Estoque
     - stock
     - Number
     - Informa a quantidade em estoque da SKU do produto
   * - Informações Adicionais
     - extra_info
     - Object
     - Mapa com informações adicionais que não coincidem com nenhum outro campo deste objeto sku do produto. Chaves: nome extra. Valores: valores extra. Exemplos: *new*, *promo*, *free_shipping*, *black_friday*, etc

Exemplo:

.. code-block:: json

       "skus": [
        {
            "sku":"203458",
            "specs": {
                "tamanho":"034",
                "cor":"58"
            },
            "status":"available",
            "old_price":499.99,
            "price":379.99,
            "installment": {
                "count":12,
                "price":31.67
            },
            "extra_info": {
                "new":false
            },
            "images":{
                "default":"/180x180/203458.jpg"
            }
        },
        {
            "sku":"203558",
            "specs": {
                "tamanho":"035",
                "cor":"58"
            },
            "status":"available",
            "old_price":499.99,
            "price":379.99,
            "installment": {
                "count":12,
                "price":31.67
            },
            "extra_info": {
                "new":false
            },
            "images": {
                "default":"/180x180/203558.jpg"
            }
        }
        ]
        
- **Kits de Produtos (product kit):** Quando um produto é um grupo de outros produtos, ele se torna um kit. Um kit contém muitos produtos que devem ser ordenados como o valor da propriedade **kit_products**.


Cart
----

A propriedade chamada **cart** deve ser inserida em **metadados_recsys**. 

Deve ser incluída na páginas de **carrinho** do e-commerce. Essa propriedade contém informações sobre todos os produtos inseridos no carrinho atual e deve ser criada da seguinte maneira:

.. list-table:: Propriedades do tipo *cart*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição     
   * - Itens*
     - items
     - Array de Item
     - Lista de objetos do tipo Item com os produtos inseridos no carrinho
   * - Produto*
     - product
     - Object Product Cart
     - Detalhe do produto que está no carrinho
   * - Id do Produto*
     - id
     - String
     - Id do produto inserido no carrinho
   * - Preço do Produto*
     - price
     - Number
     - Informa o preço do produto que está no carrinho. Este campo deve ser preenchido usando o ponto (.) como o separador decimal, sem o separador de milhares, e não deve exibir o marcador de moeda ($)
   * - SKU do Produto*
     - sku
     - String
     - Código da SKU do produto que está no carrinho
   * - Quantidade*
     - quantity
     - Number
     - Quantidade de itens do produto no carrinho
     
Exemplo:

.. code-block:: json

    cart: {
        items: [
        {
            product: {
                id: "882762",
                price: 34.99,
                sku: "882762324996"
            },
            quantity: 1
        },
        {
            product: {
                id: "2222",
                price: 114.99,
                sku: "33333"
            },
            quantity: 2
        }
        ]
    }


Transaction
-----------

A propriedade chamada **transaction** deve ser inserida em **metadados_recsys**. 

Deve ser incluída na páginas de **confirmação** do fechamento de um pedido do e-commerce. Essa propriedade contém informações sobre o pedido realizado pelo usuário e deve ser criada da seguinte maneira:

.. list-table:: Propriedades do tipo *confirmation*
   :widths: auto
   :header-rows: 1

   * - Nome
     - Chave
     - Tipo
     - Descrição
   * - Id do Pedido*
     - id
     - String
     - Identificador único e exclusivo da transação do pedido
   * - Produtos comprados*
     - items
     - Array de Items
     - Lista de itens do carrinho adquirido. Cada item é um objeto que contém o produto, a quantidade e a lista de tags
   * - Informações de entrega
     - shipping
     - Object Shipping
     - Informações de entrega do pedido. Este objeto contém custo, data de entrega e *tracking*
   * - Valores de serviços
     - services
     - Number
     - Valor de serviços aplicado na compra. Exemplo: garantia, instalação
   * - Assinatura*
     - signature
     - String
     - Assinatura da transação do pedido. Gerada através da combinação de campos com um Hash MD5

     
**Assinatura:**
Este passo é muito importante para assegurar que os dados transmitidos sejam consistentes e o receptor é quem é afirma ser. É por isso que a propriedade da assinatura é essencial.

Para obter a assinatura, é necessário concatenar algumas propriedades da transação com a chave secreta e depois gerar o hash MD5. Em seguida, é mostrado passo a passo para gerar a assinatura do último exemplo.

.. code-block:: java
    
    //Obs: Os campos devem ser concatenados no seguinte formato:
    "orderId:secretId:item1Id,item1Sku,item1Price,item1Quantity:item2Id,item2Sku,item2Price,item2Quantity:..."

Exemplo:

.. code-block:: json

    "transaction": {
        "id":"156144",
        "items": [
        {
            "product": {
                "id":"2",
                "price":169.99,
                "sku":"203458"
            },
            "quantity":1
        }
        ],
        "signature":"bbd04e1a8479a12a8f076a578ec9aecb"
    }
