﻿Metadados
=========

A aplicação receberá a informação do e-commerce através da variável global **metadados_recsys**. Esta informação nos permite rastrear e analisar todas as interações do usuário nas páginas da aplicação. Como consequência, as seguintes etapas devem ser implementadas em todos os página no site. 

Mais especificamente, a variável global **metadados_recsys** deve ser definida com as respectivas informações, conforme explicado neste documento. Esta variável **deve** ser posicionada dentro da *head tag* e acima da *tag script*, que chama **recsys.js**.

As próximas sessões explicam cada propriedade e todos os campos contidos na variável. **Cada campo com * é obrigatório**.
