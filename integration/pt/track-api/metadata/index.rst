﻿Integração de scripts e metadados
=================================

A integração de metadados deve ocorrer em ambos os sites (www e m.), além do aplicativos para dispositivos móveis (Android e/ou IOS), caso existam. 

Todas as visualizações de página do site ou aplicativo devem chamar a API Biggy REST para garantir a ingestão de dados em tempo real. Os dados enviados nesta integração são chamados de **metadados** e serão detalhados posteriormente.

Para a integração do site, podemos usar uma das três abordagens diferentes:

1. Plugin da sua plataforma de comércio eletrônico (por exemplo, VTEX, Magento, etc.), quando disponível;

2. Implementação no código-fonte do seu comércio eletrônico;

3. Gerenciador de tags do Google (*Google Tag Manager* - GTM).

Para a integração de aplicativos para dispositivos móveis, Biggy oferece uma SDK e um Framework para plataformas Android e IOS, respectivamente.

Independentemente da abordagem, Biggy fornece uma equipe de desenvolvedores para ajudá-lo na transição e implementação de integração de dados e definição de layout.

Em resumo, esses métodos de integração realizam duas tarefas essenciais:

- Inserir um script que chama Biggy API;
- Criar a variável chamada "metadados_recsys" em cada página de comércio eletrônico (detalhado posteriormente). Vale ressaltar que essa variável 0tem uma estrutura única para cada tipo de página, como: casa, detalhes do produto, carrinho, categoria, check-out e confirmação.
