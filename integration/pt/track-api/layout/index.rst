﻿Desenvolvimento de Layout
=========================

O layout e a apresentação são realizados pelos desenvolvedores web da Biggy usando seu layout do comércio eletrônico como base/referência e assim mantém conformidade entre ambos. 

Vale ressaltar que o **b-mail**, **b-api** e **b-front** possuem algumas singularidades quando se trata de layout implementação:

- **b-mail:** o desenvolvimento de todos os HTMLs para cada campanha de e-mail que será enviada ao seu usuário. A equipe da Biggy garante um HTML responsivo que será apresentado com alta qualidade no principais serviços de webmail do mercado.

- **b-api:** esse serviço possui dois tipos:
    - *b-api campaign*: o desenvolvimento de um construtor de imagem que usa um modelo para criar imagens de produtos que estará disponível através da nossa API de recomendação (geralmente usada no marketing por e-mail).
    - *b-api on demand:* não há o desenvolvimento de layout para esse serviço, pois o mesmo retorna apenas as informações das recomendações (sem templates)
    
- **b-front:** o desenvolvimento de todos os HTMLs que serão renderizados no e-commerce (ambos www e m. versões);

Basicamente, reuniões com ambas as equipes para definições de layout são necessárias para iniciar o desenvolvimento.
