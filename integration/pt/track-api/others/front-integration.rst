﻿Integração do B-Front
=====================

Etapas
------

A integração com o serviço da Biggy chamado **b-front** consiste em dois passos principais:

1. Importar o script *loader.js* dentro do *header* HTML do e-commerce;

2. Criar e posicionar as tags <div> em áreas e páginas onde a frente da loja será exibida. As páginas disponíveis são: 
    - home
    - product
    - detail
    - cart
    - category
    - search
    - checkout
    - confirmation
    
Exemplo:

.. code-block:: html

    <head>
        <script src='<LOADER_JS_URL>' biggy-key="'<BIGGY_KEY>'"></script>
    </head>
    
    <body>
        <div id="biggy-one"   biggy-area="one"></div>
        <div id="biggy-two"   biggy-area="two"></div>
        <div id="biggy-three" biggy-area="three"></div>
        <div id="biggy-four"  biggy-area="four"></div>
        <div id="biggy-five"  biggy-area="five"></div>
    </body>
    


Depois de efetuar os dois passos citados, o **b-front** estará disponível e será renderizado nas vitrines e páginas requisitadas.


Script das vitrines
-------------------

Caso o metadado esteja montado no body, indica-se chamar com autorun habilitado.

Para lojas especiais, com metadados async ou ajax, é necessário desabilitar o autorun e chamar manualmente assim que o metadados estiver disponível.

.. code-block:: html

    <script src="'<LOADER_JS_URL>'" biggy-key="'<BIGGY_KEY>'" biggy-autorun="false"></script>
    

.. note:: As configurações podem ser informadas em outro bloco <script>. Segue abaixo o exemplo.

.. code-block:: html

    <script>
        window.biggyFrontKey = '<BIGGY_KEY>';
        window.biggyFrontAutorun = false;
    </script>

    <script src="'<LOADER_JS_URL>'"></script>
    
Caso seja optado por informar as configurações em outro bloco, para realizar a a chamada manualmente basta executar o seguinte código:

.. code-block:: html

    window.biggyFrontRunAsync();
