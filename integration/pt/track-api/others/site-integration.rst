﻿Integração do Site
==================

Conforme citado anteriormente, para a integração do site, exitem três abordagens diferentes possíveis:

1. Plugin da Plataforma
-----------------------

A Biggy fornece um plugin para as principais plataformas de comércio eletrônico, geralmente compatível com as versões mais recentes. 

Verifique com nossa equipe se há um plugin disponível para sua plataforma compatível com a versão usada no seu e-commerce.


2. Implementação no código-fonte do seu comércio eletrônico
-----------------------------------------------------------

A Biggy fornece uma equipe de desenvolvimento para implementar os metadados e as chamadas da API diretamente no código-fonte do seu comércio eletrônico, seguindo exatamente o que foi apresentado neste documento.

Se houver quaisquer restrições de segurança para acessar o código-fonte do seu comércio eletrônico, a Biggy também fornece uma equipe para responder qualquer dúvida que você possa ter sobre a implementação.


3. Gerenciador de tags do Google (*Google Tag Manager* - GTM)
-------------------------------------------------------------

A ferramenta do Gerenciador de tags do Google (o *Google Tag Manager* - GTM) pode ser usada para fazer a integração.

Usando o conceito de camada de dados (**data layer**), é possível criar **macros** dentro do GTM para preencher o metadados da Biggy, **desde que todas as informações** já existam no *data layer*. É necessário que todos os atributos sejam preenchidos
exatamente como descrito nesta documentação.

Para chamar a API, basta criar uma **tag** dentro do GTM e garantir que **todas as páginas** o renderizarão. Esta *tag* deve ter o código fornecido na sessão "Recsys Script" e deve ser executado após o metadado ser carregado pelo *data layer* e *macros*.

