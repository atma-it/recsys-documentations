﻿Injeção de Dados Passados
=========================

A injeção de dados passada é uma técnica aplicada a uma nova loja integrada Biggy. Este processo está destinado para integrar os dados passados ​​antes da integração com o Biggy. Desta forma, será possível usar informações que não puderam ser capturadas pela integração porque já haviam acontecido. O resultado disso é o enriquecimento da base de dados e a melhoria do desempenho da recomendação.

Para mais informações, consulte a equipe da Biggy.
