﻿Primeiros passos
================

A plataforma de recomendação do Biggy tem 3 produtos: **b-mail**, **b-api** e **b-front**. Independentemente do produto que será utilizado, a integração do Biggy ocorre em 2 etapas principais:

 1. Definição e construção do layout;
 2. Implementação de metadados.
 
O tópico *Layout* (detalhado na seção "Desenvolvimento") aborda como implementar a apresentação de layout de recomendação para b-mail, b-api e b-front. 
A Biggy fornece uma equipe de desenvolvimento para implementar todo o metadado de integração e recomendação e layout para b-mail, b-api e b-front. Normalmente, entre 3 a 5 semanas são necessárias para alimentar os bancos de dados da Biggy com as informações do seu e-commerce e desenvolver todos os layouts necessários.

O tópico "Metadados" aborda sobre a integração de todas as suas visualizações de páginas do comércio eletrônico com Biggy através de uma API REST. Esta integração permite a análise comportamental de usuários e o mecanismo de recomendação processa com
alto desempenho e assertividade.
