﻿B-API Campaign
==============

O **b-api campaign** possui duas APIs que devem ser injetadas diretamente no seu código HTML para que seja criada a vitrine de recomendação. Essas APIs retornam a *imagem* e *link* do produto recomendado, facilitando a integração em e-mails marketing e blogs.

Exemplo de utilização do b-api campaign: 

.. code-block:: html

    <a href="http://<dominio-biggy-api>/rec-api/v1/<store-id>/link/0?email=<email-usuario-alvo>&uniqueId=<unique-id>">
        <img src="http://<dominio-biggy-api>/rec-api/v1/<storeId>/image/0?email=<email-usuario-alvo>&templateId=<templateid>&uniqueId=<unique-id>">
    </a>
    

Exemplo com os valores preenchidos para uma vitrine com 3 produtos: 

.. code-block:: html
    
    <a href="http://api.biggylabs.com/rec-api/v1/123456/link/0?email=alan@biggy.com.br&uniqueId=natal_27012016">
        <img src=" http://api.biggylabs.com/rec-api/v1/123456/image/0?email=alan@biggy.com.br&templateId=987654&uniqueId=natal_27012016">
    </a>
    <a href="http://api.biggylabs.com/rec-api/v1/123456/link/1?email=alan@biggy.com.br&uniqueId=natal_27012016">
        <img src=" http://api.biggylabs.com/rec-api/v1/123456/image/1?email=alan@biggy.com.br&templateId=987654&uniqueId=natal_27012016">
    </a>
    <a href="http://api.biggylabs.com/rec-api/v1/123456/link/2?email=alan@biggy.com.br&uniqueId=natal_27012016">
        <img src=" http://api.biggylabs.com/rec-api/v1/123456/image/2?email=alan@biggy.com.br&templateId=987654&uniqueId=natal_27012016">
    </a>

**Observação:** Note que para cada produto a ser exibido, a chamada ao índice (**0**, **1**, **2**...) é o que sofre variação.

A tag <a> preenche o atributo "href" utilizando o *endpoint* **link**.

A tag <img> preenche o atributo "src" utilizando o *endpoint* **image**.

Nos *endpoints* **link** e **image** são obrigatórios os seguintes parâmetros:

- **storeId:** identificador único da loja.
- **email:** email do usuário alvo.
- **uniqueId:** identificador único da sua campanha de e-mail. Sugerimos utilizar o nome da campanha concatenado com a data do envio (ex.: natal_27012017). Caso a campanha não possua nome, utilize somente a data do envio (obs.: esse campo é utilizado somente para exportar relatórios de desempenho pelo console administrativo da Biggy). 

No *endpoint* **image** é possível informar qual template deve ser apresentado na vitrine utilizando o parâmetro templateId, que pode ser obtido através do console administrativo. Caso o templateId não for informado, um template padrão será utilizado na apresentação da vitrine.

Além disso, é possível criar e editar novos templates de vitrine e configurar atributos como: local de preço de/por, tamanho e cor de fonte e tamanho de imagens, cor de fundo etc. Você pode criar quantos templates desejar e utilizá-los conforme a demanda (ex.: e-mails marketing com design diferente). 

Outro parâmetro que pode ser utilizado é o **random** (true / false - opcional). Ele é utilizado caso seja necessário fazer uma variação na ordem das recomendações do cliente.

Outros parâmetros opcionais que podem ser passados nos *endpoints*, e devem ser iguais tanto para o *endpoint* **image** e **link** da mesma recomendação:

- **strategy:** Estratégia a ser usada para as recomendações, relação de estratégias disponíveis abaixo.
- **categoryId:** Caso a estratégia seja de recomendação para categorias, passando o id de uma categoria, o retorno do b-api será de produtos da categoria informada. Caso não possua recomendação para a categoria informada, retorna a recomendação para a mesma estratégia mas aberto para todos os produtos da loja.
- **productId:** Caso a estratégia seja de recomendação para produtos, passando o id de um produto, o retorno do b-api será de produtos recomendados baseando-se no produto passado como base. Caso a estratégia seja de recomendação para categorias, o retorno do b-api será de produtos da mesma categoria que o produto passado.
- **campaignId:** Caso a estratégia seja de recomendação para campanhas, passando o id de uma campanha, o retorno do b-api será de produtos da campanha informada.
- **tags:** Uma lista de tags, separadas por vírgula, o retorno do b-api será de produtos que possuam pelo menos uma das tags informadas.

Caso a estratégia passada seja de recomendação para categorias, e só seja passada o email do usuário, o retorno do b-api será de produtos da última categoria acessada pelo usuário. Caso nenhum dos caminhos de recomendação explicados acima se aplique, o retorno do b-api seguirá o caminho padrão, com recomendações baseadas no interesse do usuário.

Exemplo de utilização do b-api campaign com outro caminho de recomendação:

.. code-block:: html
    
    <a href="http://<dominio-biggy-api>/rec-api/v1/<store-id>/link/0?productId=<id-produto>&uniqueId=<unique-id>&strategy=MVW">
        <img src="http://<dominio-biggy-api>/rec-api/v1/<storeId>/image/0?productId=<id-produto>&templateId=<template-id>&uniqueId=<unique-id>&strategy=MVW">
    </a>

A seguir uma tabela de estratégia, o valor que deve ser passado no parâmetro *strategy* para utilizá-las e quais são suas possíveis entradas:

.. list-table:: Estratégias para o b-api campaign
   :widths: auto
   :header-rows: 1

   * - Nome da Estratégia
     - Strategy     
     - User
     - Category
     - Product
     - Campaign
   * - Best Sellers
     - BST
     - ✓
     - ✓
     - ✓
     - ✓
   * - Most Viewed
     - MVW
     - ✓
     - ✓
     - ✓
     - ✓
   * - Last Price Reduction
     - LPR
     - ✓
     - ✓
     - ✓
     - ✓
   * - New Releases
     - NEW
     - ✓
     - ✓
     - ✓
     - ✓
   * - Similar Products
     - SCO
     - 
     - 
     - ✓
     - 
   * - Best Choice
     - BCH
     - 
     - 
     - ✓
     - 
   * - Bought Together
     - GCMP
     - 
     - 
     - ✓
     - 
   * - User Navigation
     - NAV
     - ✓
     - 
     - 
     - 
     

