﻿B-API On Demand
===============

O **b-api on demand** fornece recomendações personalizadas baseadas em **estratégias** (que serão descritas a seguir), atendendo a demanda em diversos cenários, desde páginas em aplicativos mobile até sistemas em lojas físicas. 

Para mais detalhes sobre as possibilidades de retorno, acessar a seção :ref:`Características da Recomendação <api-ondemand-features-page>`

Essa API consiste em três chamadas principais descritas abaixo:
    - Recommendation Request
    - Recommendation View
    - Recommendation Click
    
Recommendation Request
----------------------

O Recommendation Request é responsável por submeter todos os dados que serão necessários para a API de recomendação e é obrigatório em todas as chamadas. 

Um *endpoint* **único** atende a todas as possibilidades de recomendação, com a seguinte estrutura:

.. code-block:: json

    POST http://<dominio-biggy-api>/rec-api/v1/<store-id>/ondemand/<strategy>
    
    {
        "origin": "product", 
        "channel": "app",
        "products": [
            "4567"
        ], 
        "user": "1234",
        "minProducts": 2,
        "maxProducts": 12,
        "mergeRecommendationList": true,
        "mixMergedRecommendations": false
    }

    
Para mais detalhes sobre as propriedades (parâmetros), acessar a seção :ref:`Propriedades do Recommendation Request <api-ondemand-request-properties-page>` 

Desta forma, a requisição do **b-api on demand** é realizada e obtêm-se um JSON de retorno formado por 4 listas:

    1. baseIds
    2. baseItems
    3. recommendationIds
    4. recommendationItems
    
Para mais detalhes sobre o retorno, acessar a seção :ref:`Retorno do Recommendation Request <api-ondemand-request-return-page>` 

A seguir um exemplo do retorno da chamada da recomendação:

.. code-block:: json

    [
    {
        "baseIds": [
            "123456"
        ],
        "baseItems": [
            {
                "productId": "123456",
                "offers": [
                    {
                        "offerId": "1234560O0395",
                        "originalProductId": "123456",
                        "sku": "1234560O0395",
                        "name": "Chuteira de Campo Dry Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-dry-adulto.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/12345600.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "oldPrice": "389,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO",
                                "type": "COLOR",
                                "offerId": "1234560O0395",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "1234560O0395",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "Dry",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "123456080440",
                        "originalProductId": "123456",
                        "sku": "123456080440",
                        "name": "Chuteira de Campo adidas X 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-3-fg-adulto-123456.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/12345608.jpg",
                        "secondaryImageUrl": "",
                        "price": "269,99",
                        "oldPrice": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "27",
                            "count": "10"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 22,
                        "specs": [
                            {
                                "id": "08",
                                "label": "AMARELO",
                                "type": "COLOR",
                                "offerId": "123456080440",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "044",
                                        "label": "44",
                                        "type": "SIZE",
                                        "offerId": "123456080440",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "123456080402",
                        "originalProductId": "123456",
                        "sku": "123456080402",
                        "name": "Chuteira de Campo adidas X 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-3-fg-adulto-123456.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/12345608.jpg",
                        "secondaryImageUrl": "",
                        "price": "269,99",
                        "oldPrice": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "27",
                            "count": "10"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 22,
                        "specs": [
                            {
                                "id": "08",
                                "label": "AMARELO",
                                "type": "COLOR",
                                "offerId": "123456080402",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "040",
                                        "label": "40",
                                        "type": "SIZE",
                                        "offerId": "123456080402",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "1234560O0418",
                        "originalProductId": "123456",
                        "sku": "1234560O0418",
                        "name": "Chuteira de Campo adidas X 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-3-fg-adulto-123456.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/1234560O.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "oldPrice": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "1234560O0418",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "041",
                                        "label": "41",
                                        "type": "SIZE",
                                        "offerId": "1234560O0418",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    }
                ],
                "specs": [
                    {
                        "id": "08",
                        "label": "AMARELO",
                        "type": "COLOR",
                        "offerId": "123456080402",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "040",
                                "label": "40",
                                "type": "SIZE",
                                "offerId": "123456080402",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "123456080440",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    },
                    {
                        "id": "0O",
                        "label": "PRETO/LARANJA ESC",
                        "type": "COLOR",
                        "offerId": "1234560O0395",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "1234560O0395",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "040",
                                "label": "40",
                                "type": "SIZE",
                                "offerId": "1234560O0401",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "041",
                                "label": "41",
                                "type": "SIZE",
                                "offerId": "1234560O0418",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "042",
                                "label": "42",
                                "type": "SIZE",
                                "offerId": "1234560O0425",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "043",
                                "label": "43",
                                "type": "SIZE",
                                "offerId": "1234560O0432",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "1234560O0449",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    }
                ]
            }
        ],
        "recommendationIds": [
            "987654",
            "345602"
        ],
        "recommendationItems": [
            {
                "productId": "987654",
                "score": 26,
                "offers": [
                    {
                        "offerId": "987654J40444",
                        "originalProductId": "987654",
                        "sku": "987654J40444",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 26,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40444",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "044",
                                        "label": "44",
                                        "type": "SIZE",
                                        "offerId": "987654J40444",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "987654J40413",
                        "originalProductId": "987654",
                        "sku": "987654J40413",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 860712,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40413",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "041",
                                        "label": "41",
                                        "type": "SIZE",
                                        "offerId": "987654J40413",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "987654J40390",
                        "originalProductId": "987654",
                        "sku": "987654J40390",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 860712,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40390",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "987654J40390",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "987654J40420",
                        "originalProductId": "987654",
                        "sku": "987654J40420",
                        "name": "Chuteira de Campo adidas Nemeziz 17.3 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-nemeziz-17-3-fg-adulto-987654.html?cor=J4",
                        "imageUrl": "http://lojatesteimages.com/180x180/987654J4.jpg",
                        "secondaryImageUrl": "",
                        "price": "349,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "29.17",
                            "count": "12"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 860712,
                        "specs": [
                            {
                                "id": "J4",
                                "label": "LARANJA ESC/PRETO",
                                "type": "COLOR",
                                "offerId": "987654J40420",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "042",
                                        "label": "42",
                                        "type": "SIZE",
                                        "offerId": "987654J40420",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat882304",
                                "createdAt": "2017-08-20 02:29 AM UTC",
                                "updatedAt": "2017-08-20 02:29 AM UTC",
                                "storeId": "loja123456",
                                "name": "Nemeziz",
                                "parent": "cat632000",
                                "originalId": "632510",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    }
                ],
                "specs": [
                    {
                        "id": "J4",
                        "label": "LARANJA ESC/PRETO",
                        "type": "COLOR",
                        "offerId": "987654J40390",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "987654J40390",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "041",
                                "label": "41",
                                "type": "SIZE",
                                "offerId": "987654J40413",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "042",
                                "label": "42",
                                "type": "SIZE",
                                "offerId": "987654J40420",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "987654J40444",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    }
                ]
            },
            {
                "productId": "345602",
                "score": 25,
                "offers": [
                    {
                        "offerId": "3456020O0416",
                        "originalProductId": "345602",
                        "sku": "3456020O0416",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 25,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0416",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "041",
                                        "label": "41",
                                        "type": "SIZE",
                                        "offerId": "3456020O0416",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0447",
                        "originalProductId": "345602",
                        "sku": "3456020O0447",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0447",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "044",
                                        "label": "44",
                                        "type": "SIZE",
                                        "offerId": "3456020O0447",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0393",
                        "originalProductId": "345602",
                        "sku": "3456020O0393",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0393",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "3456020O0393",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0409",
                        "originalProductId": "345602",
                        "sku": "3456020O0409",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0409",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "040",
                                        "label": "40",
                                        "type": "SIZE",
                                        "offerId": "3456020O0409",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0386",
                        "originalProductId": "345602",
                        "sku": "3456020O0386",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0386",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "038",
                                        "label": "38",
                                        "type": "SIZE",
                                        "offerId": "3456020O0386",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0430",
                        "originalProductId": "345602",
                        "sku": "3456020O0430",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0430",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "043",
                                        "label": "43",
                                        "type": "SIZE",
                                        "offerId": "3456020O0430",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "345602080387",
                        "originalProductId": "345602",
                        "sku": "345602080387",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/34560208.jpg",
                        "secondaryImageUrl": "",
                        "price": "179,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.71",
                            "count": "7"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 21,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "08",
                                "label": "AMARELO",
                                "type": "COLOR",
                                "offerId": "345602080387",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "038",
                                        "label": "38",
                                        "type": "SIZE",
                                        "offerId": "345602080387",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "345602460394",
                        "originalProductId": "345602",
                        "sku": "345602460394",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/34560246.jpg",
                        "secondaryImageUrl": "",
                        "price": "179,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.71",
                            "count": "7"
                        },
                        "hasDiscount": true,
                        "discountPercentage": 21,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "46",
                                "label": "BRANCO/CINZA",
                                "type": "COLOR",
                                "offerId": "345602460394",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "039",
                                        "label": "39",
                                        "type": "SIZE",
                                        "offerId": "345602460394",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    },
                    {
                        "offerId": "3456020O0423",
                        "originalProductId": "345602",
                        "sku": "3456020O0423",
                        "name": "Chuteira de Campo adidas X 17.4 FG - Adulto",
                        "url": "www.lojateste.com.br/chuteira-de-campo-adidas-x-17-4-fg-adulto-345602.html?cor=08",
                        "imageUrl": "http://lojatesteimages.com/180x180/3456020O.jpg",
                        "secondaryImageUrl": "",
                        "price": "229,99",
                        "oldPrice": "229,99",
                        "currencySymbol": "R$",
                        "installment": {
                            "price": "25.55",
                            "count": "9"
                        },
                        "hasDiscount": false,
                        "discountPercentage": 0,
                        "score": 824019,
                        "specs": [
                            {
                                "id": "0O",
                                "label": "PRETO/LARANJA ESC",
                                "type": "COLOR",
                                "offerId": "3456020O0423",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": [
                                    {
                                        "id": "042",
                                        "label": "42",
                                        "type": "SIZE",
                                        "offerId": "3456020O0423",
                                        "images": null,
                                        "imagesSsl": null,
                                        "subSpecs": null
                                    }
                                ]
                            }
                        ],
                        "categories": [
                            {
                                "id": "cat123456",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Futebol",
                                "parent": null,
                                "originalId": "112233",
                                "ancestors": []
                            },
                            {
                                "id": "cat147258",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "CALCADOS",
                                "parent": "cat123456",
                                "originalId": "223344",
                                "ancestors": [
                                    "cat123456"
                                ]
                            },
                            {
                                "id": "cat632000",
                                "createdAt": "2017-08-20 02:21 AM UTC",
                                "updatedAt": "2017-08-20 02:21 AM UTC",
                                "storeId": "loja123456",
                                "name": "Chuteira De Campo",
                                "parent": "cat147258",
                                "originalId": "556677",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258"
                                ]
                            },
                            {
                                "id": "cat771122",
                                "createdAt": "2017-08-20 02:38 AM UTC",
                                "updatedAt": "2017-08-20 02:38 AM UTC",
                                "storeId": "loja123456",
                                "name": "X",
                                "parent": "cat632000",
                                "originalId": "889900",
                                "ancestors": [
                                    "cat123456",
                                    "cat147258",
                                    "cat632000"
                                ]
                            }
                        ]
                    }
                ],
                "specs": [
                    {
                        "id": "08",
                        "label": "AMARELO",
                        "type": "COLOR",
                        "offerId": "345602080387",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "038",
                                "label": "38",
                                "type": "SIZE",
                                "offerId": "345602080387",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    },
                    {
                        "id": "0O",
                        "label": "PRETO/LARANJA ESC",
                        "type": "COLOR",
                        "offerId": "3456020O0423",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "038",
                                "label": "38",
                                "type": "SIZE",
                                "offerId": "3456020O0386",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "3456020O0393",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "040",
                                "label": "40",
                                "type": "SIZE",
                                "offerId": "3456020O0409",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "041",
                                "label": "41",
                                "type": "SIZE",
                                "offerId": "3456020O0416",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "042",
                                "label": "42",
                                "type": "SIZE",
                                "offerId": "3456020O0423",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "043",
                                "label": "43",
                                "type": "SIZE",
                                "offerId": "3456020O0430",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            },
                            {
                                "id": "044",
                                "label": "44",
                                "type": "SIZE",
                                "offerId": "3456020O0447",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    },
                    {
                        "id": "46",
                        "label": "BRANCO/CINZA",
                        "type": "COLOR",
                        "offerId": "345602460394",
                        "images": null,
                        "imagesSsl": null,
                        "subSpecs": [
                            {
                                "id": "039",
                                "label": "39",
                                "type": "SIZE",
                                "offerId": "345602460394",
                                "images": null,
                                "imagesSsl": null,
                                "subSpecs": null
                            }
                        ]
                    }
                ]
            }
        ]
    }
    ]
  

Recommendation View
-------------------

O Recommendation View permite avaliar por meio do Dashboard da Biggy o desempenho da recomendação requisitada em relação à sua visualização. O objetivo é efetuar essa chamada quando o usuário "ver" a recomendação.

A seguir um exempo da chamada do Recommendation View:

.. code-block:: json

    POST http://<dominio-biggy-api>/rec-api/v1/<store-id>/ondemand/<strategy>/view
    
    {
        "origin": "product", 
        "session": "session123456"
    }

.. note:: Os atributos *strategy* e *origin* seguem a mesma estrutura existente para o Recommendation Request. Para mais detalhes, acessar as seções :ref:`Estratégias <api-strategy-page>` e :ref:`Origem <api-origin-page>`


Recommendation Click
--------------------

O Recommendation Click permite avaliar por meio do Dashboard da Biggy o desempenho da recomendação requisitada em relação ao disparo de eventos de clique. O objetivo é efetuar a chamada desse método quando o usuário "clicar" em algum produto da recomendação.

A seguir um exempo da chamada do Recommendation Click:

.. code-block:: json

    POST http://<dominio-biggy-api>/rec-api/v1/<store-id>/ondemand/<strategy>/click
    
    {
        "origin": "product", 
        "session": "session123456",
        "productId": "123456"
    }

.. note:: Os atributos *strategy* e *origin* seguem a mesma estrutura existente para o Recommendation Request. Para mais detalhes, acessar as seções :ref:`Estratégias <api-strategy-page>` e :ref:`Origem <api-origin-page>`
