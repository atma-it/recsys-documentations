﻿B-API
=====

Com o objetivo de integrar-se em diversas plataformas e possibilitar a personalização em diferentes contextos, a Biggy disponibiliza o **b-api**, um serviço de recomendação via API que possui duas abordagens:

 - **B-API Campaign:** APIs que retornam recomendações personalizadas por usuário prontas para integrar-se em e-mail marketing e blogs, com chamadas facilitadas para montagem de vitrines sem necessidade de envolvimento com a área de TI.
 
 - **B-API On Demand:** APIs preparadas para serem consumidas em qualquer contexto, desde aplicativos mobile até sistemas de lojas físicas. As recomendações levam em consideração o cliente, categoria, produto e loja.
