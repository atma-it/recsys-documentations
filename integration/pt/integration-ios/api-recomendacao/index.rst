﻿API de recomendação para IOS
============================

A Biggy Framework fornece recomendações personalizadas com uma variedade de **estratégias** que utilizam **parâmetros** para processar e retornar uma lista de **itens de recomendação**. 

Para mais detalhes sobre as possibilidades de retorno, acessar a seção :ref:`Características da Recomendação <api-ondemand-features-page>`

As chamadas para as APIs de Track e Recomendação são realizadas a partir do objeto **BGYBiggyClient**, objeto responsável por montar o corpo da requisição e parsear o retorno obtido. 

Para instanciar uma versão compartilhada do objeto, basta passar como parâmetro um objeto do tipo [String:AnyObject] instanciado a partir de um arquivo **plist** contendo as propriedades apresentadas no item anterior, ou passar o caminho para o arquivo, como apresentado:

.. code-block:: swift

    if let path = Bundle.main.path(forResource: "Biggy", ofType: "plist") {
        try? BGYBiggyClient.instantiate(contentsOfFile: path)
    }

Para utilizar o objeto é possível utilizar a propriedade **shared**. Atente-se que a função **instantiate** deve ter sido chamada anteriormente, visto que o objeto shared é de tipo **"BGYBiggyClient?"**. 

Essa API consiste em três métodos principais descritos abaixo:
    - Recommendation Request
    - Recommendation View
    - Recommendation Click

Recommendation Request
----------------------

A estrutura **BGYRecommendationMetadata** é responsável por encapsular todos os dados que serão submetidos para a API de recomendação e é obrigatória em todas as chamadas.

.. code-block:: objectivec

    @objc public class BGYRecommendationMetadata: NSObject, Mappable {
    
        // Required
        public var strategy: BGYStrategy!
        var sessionId: String? // required but automatically set by BiggyClient
        
        // Optional
        public var origin: BGYOrigin?
        public var user: String?
        public var products: [String]?
        public var categories: [String]?
        public var anonymousUser: String?
        
        public init(strategy: BGYStrategy) {
            self.strategy = strategy
        }
    }
    
    @objc public enum BGYStrategy: Int, RawRepresentable {
        case offers_store
        case offers_category
        case offers_user
        case most_viewed_store
        case most_viewed_category
        case most_viewed_campaign
        case most_viewed_user
        case best_sellers_store
        case best_sellers_category
        case best_sellers_campaign
        case best_sellers_user
        case new_releases_store
        case new_releases_category
        case new_releases_user
        case click_history
        case navigation_history
        case order_history
        case cart_abandonment
        case bought_together
        case best_choice
        case similar_products
    }
    
    @objc public enum BGYOrigin: Int, RawRepresentable {
        case home
        case product
        case product_unavailable
        case category
        case campaign
        case cart
        case empty_cart
        case checkout
        case confirmation
        case search
        case empty_search
        case not_found
        case order_list
        case order_detail
        case wishlist
        case internals
    }

Para mais detalhes sobre as propriedades (parâmetros), acessar a seção :ref:`Propriedades do Recommendation Request <api-ondemand-request-properties-page>` 


O método **recommendation** da classe **BGYBiggyClient** é responsável por realizar a chamada à API. Esse método recebe um BGYRecommendationMetadata e uma função como parâmetros, a função completion é chamada com uma array de BGYRecommendation como parâmetro, objeto que possui a seguinte estrutura:

.. code-block:: objectivec

    @objc public class BGYRecommendation: NSObject, Mappable {   
        public var baseIds: [String]?
        public var baseItems: [BGYProductRecommendation]?
        public var recommendationItems: [BGYProductRecommendation]?
        public var recommendationIds: [String]?
    }

Desta forma, o retorno do Recommendation é composto por 4 listas:

    1. baseIds
    2. baseItems
    3. recommendationIds
    4. recommendationItems
    
Para mais detalhes sobre o retorno, acessar a seção :ref:`Retorno do Recommendation Request <api-ondemand-request-return-page>` 

A seguir um exemplo da chamada da recomendação:

.. code-block:: objectivec

    try!  BGYBiggyClient.shared?.recommendation(recommendationMetadata:  recMetadata!, 
    completion: { recommendations in
        // recommendations é uma lista de BGYRecommendation
    })
    
Com a lista de bases e items de recomendação é possível montar as vitrines de acordo com a necessidade da sua app, utilizando todas as estratégias de recomendação e personalização disponibilizadas pela Biggy.

Recommendation View
-------------------

O método **recommendationView** presente no *BGYBiggyClient* permite avaliar por meio do Dashboard da Biggy o desempenho da recomendação requisitada em relação à sua visualização. O objetivo é efetuar a chamada desse método quando o usuário "ver" a recomendação. Vale ressaltar que esse método realiza a chamada  na API da Biggy de forma assíncrona, não afetando a performance da aplicação.

A seguir um exempo da chamada do recommendationView:

.. code-block:: objectivec

    try! BGYBiggyClient.shared?.recommendationView(recommendationViewMetadata: recView!)
    
O objeto **BGYRecommendationViewMetadata** é responsável por encapsular todos os dados que serão submetidos na chamada do *recommendationView*. Os campos informados são obrigatórios em todas as chamadas. A seguir a sua estrutura e uma breve descrição de cada campo:

.. code-block:: objectivec

    @objc public class BGYRecommendationViewMetadata: NSObject, Mappable {
        // Required
        public var strategy: BGYStrategy!
        
        // Optional
        public var origin: BGYOrigin?
    }

.. note:: Os atributos *strategy* e *origin* seguem a mesma estrutura existente para o Recommendation Request. Para mais detalhes, acessar as seções :ref:`Estratégias <api-strategy-page>` e :ref:`Origem <api-origin-page>`


Recommendation Click
--------------------

O método **recommendationClick** presente no *BGYBiggyClient* permite avaliar por meio do Dashboard da Biggy o desempenho da recomendação requisitada em relação ao disparo de eventos de clique. O objetivo é efetuar a chamada desse método quando o usuário "clicar" em algum produto da recomendação. Vale ressaltar que esse método realiza a chamada na API da Biggy de forma assíncrona, não afetando a performance da aplicação.

A seguir um exempo da chamada do recommendationClick:

.. code-block:: objectivec
    
    try! BGYBiggyClient.shared?.recommendationClick(recommendationClickMetadata: recClick!)

O objeto **BGYRecommendationClickMetadata** é responsável por encapsular todos os dados que serão submetidos na chamada do *recommendationClick*. Os campos informados são obrigatórios em todas as chamadas. A seguir a sua estrutura e uma breve descrição de cada campo:

.. code-block:: objectivec

    @objc public class BGYRecommendationClickMetadata: NSObject, Mappable {
        // Required
        public var strategy: BGYStrategy!
        public var productId: String!
        
        // Optional
        public var origin: BGYOrigin? 
    }


.. note:: Os atributos *strategy* e *origin* seguem a mesma estrutura existente para o Recommendation Request. Para mais detalhes, acessar as seções :ref:`Estratégias <api-strategy-page>` e :ref:`Origem <api-origin-page>`

.. note:: O atributo **productId** é uma *String* e deve informar qual o Id do produto que o usuário clicou na recomendação. **É um atributo obrigatório.**




.. note:: Caso prefira realizar as chamadas de recomendação direto dede outra maneira, é possível requisitar as recomendações chamando diretamente de sua aplicação a API de recomendação da Biggy, com retorno no formato JSON, todavia **é obrigatório** a realização da chamadas dos três métodos (Recommendation Request, Recommendation View, Recommendation Click) caso prefira essa opção. Note que realizar a chamada dessa maneira **não substitui** também a implementação da integração do track. Para mais informações, consulte a equipe da Biggy.
