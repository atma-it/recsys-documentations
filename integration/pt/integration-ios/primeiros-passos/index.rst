﻿Primeiros passos
================

Com o objetivo de  facilitar o processo de integração com aplicativos iOS, a Biggy disponibiliza uma **Framework** que abstrai chamadas em nossas APIs de Recomendação e Tracking.

Com  essa  Framework  é  possível  utilizar  todos  os  recursos  disponíveis  para  que  seja  feito  o acompanhamento de navegação do usuário, bem como solicitação de recomendações em diversos contextos. A framework foi escrita em Swift 4, porém é compátivel com o Swift 3, com Objective-C e funciona em versões posteriores à iOS 8.


Inclusão da Framework em aplicativos iOS
----------------------------------------

O arquivo ".framework" deve ser copiado para a pasta raiz do aplicativo e ser referenciado nos campos Embedded Binaries e Linked Frameworks and Libraries nas configurações do projeto.

Para importar as classes da framework em um arquivo do projeto:

.. code-block:: json

    Swift:
        import RecsysIOS
    
    
.. code-block:: json
        
    Objective-C:
        #import <RecsysIOS/RecsysIOS-Swift.h>


Configuração das propriedades da Biggy
--------------------------------------

Para o funcionamento da integração é necessário adicionar um arquivo do tipo "Property List" com algumas  variáveis  de  ambiente  que  são  utilizadas  nas  requisições  para  as  APIs  de  Tracking  e Recomendação.

Essas variáveis serão entregues pela Biggy e devem ser adicionadas da seguinte maneira:


.. image:: add-property-01.png
