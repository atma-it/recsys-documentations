 .. _api-origin-page:

Origem
======

A origem tem como objetivo informar qual o tipo da página que solicitando a recomendação. Dessa maneira, posteriormente, o Dashboard da Biggy disponibiliza métricas para o comércio eletrônico, além da possibilidade de filtro pelo tipo de página. 

Se a página não for informada, será registrada como "unknown". Essa informação é utilizada nos relatórios de desempenho, e caso não seja informada todas as requisições serão armazenadas como "unknown", afetando a avaliação das recomendações. 

Os seguintes valores são válidos:  
    - home
    - product
    - product_unavailable
    - category
    - campaign
    - cart
    - empty_cart
    - checkout
    - confirmation
    - search
    - empty_search
    - not_found
    - order_list
    - order_detail
    - internals
    - unknown
