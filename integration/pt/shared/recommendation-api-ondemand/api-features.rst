Características da Recomendação
===============================

 .. _api-ondemand-features-page:
 
A recomendação provida pela Biggy permite abordar o usuários das mais diversas maneiras, cada uma com sua particularidade e apropriada para diferentes páginas de seu e-commerce.

Dentre as diversas possibilidades, é possível obter recomendações como: 

- Produtos mais populares da loja ou categoria;
- Produtos complementares a um produto;
- Produtos similares a um produto;
- Produtos abandonados no carrinho pelo usuário, com uma lista de produtos similares;
- Produtos clicados recentemente pelo usuário, com uma lista de produtos similares;
- Produtos comprados recentemente pelo usuário, com uma lista de produtos complementares;
- Produtos mais visualizados das categorias que o usuário mais demonstrou interesse no passado;
- Produtos que são novidades das categorias que o usuário mais demonstrou interesse no passado;
