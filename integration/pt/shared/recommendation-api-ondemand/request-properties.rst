.. _api-ondemand-request-properties-page:

Propriedades do Recommendation Request
======================================

Para realizar o Recommendation Request com sucesso, é necessário que algumas propriedades sejam preenchidas. As recomendações retornadas serão orientadas por essas informações.

As propriedades existentes são:

- **Strategy:** Informa qual estratégia será utilizada para gerar a recomendação. **É um atributo obrigatório**. Para mais detalhes, acessar a seção :ref:`Estratégias <api-strategy-page>` 

- **Origin:** Informa qual o tipo da página que está solicitando a recomendação. **NÃO é um atributo obrigatório.** Para mais detalhes, acessar a seção :ref:`Origem <api-origin-page>`

- **Channel:** Informa qual o tipo de canal que está solicitando a recomendação. É aconselhável manter um padrão nas chamadas do mesmo canal (ex.: "site", "app"). **NÃO é um atributo obrigatório.**

- **User:** Informa o id do usuário logado no aplicativo que servirá como base para gerar a recomendação (ex.: busca ultimos produtos clicados pelo usuário e similares e estes). **Obrigatório dependendo da estratégia.**

- **Products:** Informa uma lista com ids dos produtos que servirão de base para gerar a recomendação (ex.: busca produtos similares ao ID do produto que está sendo visualizado). **Obrigatório dependendo da estratégia.**

- **Categories:** Informa uma lista com ids das categorias que servirão de base para gerar a recomendação (ex.: busca produtos mais vendidos nas categorias). **Obrigatório dependendo da estratégia.**

- **Campaigns:** Informa uma lista com ids das campanhas que servirão de base para gerar a recomendação (ex.: campanhas da nike e adidas de tênis de futebol). **Obrigatório dependendo da estratégia.**

- **minProducts:** Informa um número e possibilita limitar uma quantidade mínima de produtos recomendados no retorno da chamada. **NÃO é um atributo obrigatório.**

- **maxProducts:** Informa um número e possibilita limitar uma quantidade máxima de produtos recomendados no retorno da chamada. **NÃO é um atributo obrigatório.**

- **mergeRecommendationList:** Informa um valor booleano (true/false) e possibilita, quando o atributo products contém mais de um Id, que os produtos recomendados respectivos a cada Id sejam combinados e sejam retornados na mesma lista de produtos recomendados. **NÃO é um atributo obrigatório.**

- **mixMergedRecommendations:** Informa um valor booleano (true/false) e possibilita, quando o atributo products contém mais de um Id e quando o atributo mergeRecommendationList é true, que além dos produtos recomendados respectivos a cada Id serem combinados e sejam retornados na mesma lista de produtos recomendados, sejam mesclados na ordem de retorno. **NÃO é um atributo obrigatório.**
