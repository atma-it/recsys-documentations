﻿.. _api-strategy-page:

Estratégias
===========

Introdução às Estratégias
-------------------------

As estratégias tem como objetivo definir o tipo de retorno que será obtido ao realizar o request. A seguir, a listagem e a descrição de todas as estratégias e seus respectivos retornos:

Os seguintes valores são válidos: 

    - offers_store
    - offers_category
    - offers_user
    - most_viewed_store
    - most_viewed_categories
    - most_viewed_campaigns
    - most_viewed_user
    - most_viewed_products
    - best_sellers_store
    - best_sellers_categories
    - best_sellers_campaigns
    - best_sellers_user
    - best_sellers_products
    - new_releases_store
    - new_releases_categories
    - new_releases_products
    - new_releases_user
    - click_history
    - navigation_history
    - order_history
    - cart_abandonment
    - bought_together
    - best_choice
    - similar_products
    - who_clicked_also_clicked
    - who_clicked_also_purchased
    - who_purchased_also_purchased
    - promoted_store

Descrição
---------

- **offers_store:** retorna os principais produtos da loja que tiveram uma redução de preço. É recomendado utilizar essa estratégia na página home.

- **offers_category:** retorna os principais produtos da categoria informada que tiveram uma redução de preço. É recomendado utilizar essa estratégia na página de categoria. **É obrigatório informar a categoria (ou as categorias)**.

- **offers_user:** retorna os principais produtos baseados no interesse e na navegação de determinado usuário que tiveram uma redução de preço. É recomendado utilizar essa estratégia na página home. **É obrigatório informar o usuário**.

- **most_viewed_store:** retorna os produtos mais vistos na loja. É recomendado utilizar essa estratégia nas páginas home, carrinho vazio, busca vazia.

- **most_viewed_categories:** retorna os produtos mais vistos de determinada categoria informada. É recomendado utilizar essa estratégia na página de categoria. **É obrigatório informar a categoria (ou as categorias)**.

- **most_viewed_campaigns:** retorna os produtos mais vistos de determinada campanha informada. É recomendado utilizar essa estratégia na página de campanha. Se por acaso não tiver página de campanha, essa estratégia não deve ser utilizada. **É obrigatório informar a campanhas (ou as campanhas)**.

- **most_viewed_user:** retorna os produtos mais vistos baseados no interesse e na navegação de determinado usuário. É recomendado utilizar essa estratégia nas páginas home, carrinho vazio, busca vazia. **É obrigatório informar o usuário**.

- **most_viewed_products:** retorna os produtos mais vistos em relação a um determinado produto informado. É recomendado utilizar essa estratégia na página de detalhe do produto.  **É obrigatório informar o produto (ou os produtos)**.

- **best_sellers_store:** retorna os produtos mais vendidos na loja. É recomendado utilizar essa estratégia nas páginas home, carrinho vazio, busca, busca vazia.

- **best_sellers_categories:** retorna os produtos mais vendidos de determinada categoria informada. É recomendado utilizar essa estratégia na página de categoria. **É obrigatório informar a categoria (ou as categorias)**.

- **best_sellers_campaigns:** retorna os produtos mais vendidos de determinada campanha informada. É recomendado utilizar essa estratégia na página de campanha. Se por acaso não tiver página de campanha, essa estratégia não deve ser utilizada. **É obrigatório informar a campanhas (ou as campanhas)**.

- **best_sellers_user:** retorna os produtos mais vendidos baseados no interesse e na navegação de determinado usuário. É recomendado utilizar essa estratégia nas páginas home, carrinho, carrinho vazio, busca, busca vazia. **É obrigatório informar o usuário**.

- **best_sellers_products:** retorna os produtos mais vendidos em relação a um determinado produto informado. É recomendado utilizar essa estratégia na página de detalhe do produto. **É obrigatório informar o produto (ou os produtos)**.

- **new_releases_store:** retorna os lançamentos na loja, ou seja, os produtos que acabaram de entrar no catálogo da loja. É recomendado utilizar essa estratégia na página home.

- **new_releases_categories:** retorna os lançamentos da categoria, ou seja, os produtos de determinada categoria que acabaram de entrar no catálogo da loja. É recomendado utilizar essa estratégia na página de categoria. **É obrigatório informar a categoria (ou as categorias)**.

- **new_releases_products:** retorna os lançamentos em relação a um determinado produto informado. É recomendado utilizar essa estratégia nas páginas de detalhe do produto, carrinho. **É obrigatório informar o produto (ou os produtos)**.

- **new_releases_user:** retorna os lançamentos baseados no interesse e na navegação de determinado usuário, ou seja, os produtos que acabaram de entrar no catálogo da loja que foram selecionados especificamente para o usuário informado. É recomendado utilizar essa estratégia nas páginas home, carrinho, carrinho vazio, busca, busca vazia. **É obrigatório informar o usuário**.

- **click_history:** retorna os produtos recomendados respectivos aos últimos produtos que foram clicados pelo usuário na loja. É recomendado utilizar essa estratégia na página home. **É obrigatório informar o usuário**.

- **navigation_history:** retorna os últimos produtos que foram clicados pelo usuário na loja, ou seja, os produtos que o usuário entrou no detalhe do produto no site. É recomendado utilizar essa estratégia nas páginas home, detalhe do produto, carrinho vazio, carrinho, categoria, busca, busca vazia, internas (FAQs, Minha Conta, Quem Somos, etc). **É obrigatório informar o usuário**.

- **order_history:** retorna os produtos recomendados respectivos aos produtos que foram comprados pelo usuário na loja. É recomendado utilizar essa estratégia nas páginas home, carrinho vazio, carrinho. **É obrigatório informar o usuário**.

- **cart_abandonment:** retorna os produtos recomendados respectivos aos produtos que foram abandonados no carrinho da loja pelo usuário. É recomendado utilizar essa estratégia nas páginas home, carrinho. **É obrigatório informar o usuário**.

- **bought_together:** retorna os produtos complementares em relação a um determinado produto informado. É recomendado utilizar essa estratégia na página de detalhe do produto. **É obrigatório informar o produto (ou os produtos)**.

- **best_choice:** retorna os produtos que a partir de um determinado produto informado. É recomendado utilizar essa estratégia na página de detalhe do produto. Todavia, não é recomendado utilizar essa estratégia na mesma página que a estratégia *similar_products*, porque tendem a recomendar produtos semelhantes pelo fato de ambas serem baseadas em produtos (que seria o mesmo se fossem chamadas na mesma página). **É obrigatório informar o produto (ou os produtos)**.

- **similar_products:** retorna os produtos considerados mais similares a partir de um determinado produto informado. É recomendado utilizar essa estratégia na página de detalhe do produto. Todavia, não é recomendado utilizar essa estratégia na mesma página que a estratégia *best_choice*, porque tendem a recomendar produtos semelhantes pelo fato de ambas serem baseadas em produtos (que seria o mesmo se fossem chamadas na mesma página). **É obrigatório informar o produto (ou os produtos)**.

- **who_clicked_also_clicked:** retorna os produtos que são frequentemente clicados quando determinado produto informado é clicado. É recomendado utilizar essa estratégia na página de detalhe do produto. **É obrigatório informar o produto (ou os produtos)**.

- **who_clicked_also_purchased:** retorna os produtos que são frequentemente comprados quando determinado produto informado é clicado. É recomendado utilizar essa estratégia na página de detalhe do produto. **É obrigatório informar o produto (ou os produtos)**.

- **who_purchased_also_purchased:** retorna os produtos que são frequentemente comprados juntamente com a compra de determinado produto informado. É recomendado utilizar essa estratégia na página de detalhe do produto. **É obrigatório informar o produto (ou os produtos)**.

- **promoted_store:** retorna os produtos determinados pela loja para serem promovidos (destacados) quando essa estratégia for solicitada.



Propriedades
------------

Cada estratégia de recomendação necessita de um ou mais propriedades (parâmetros) para processar a requisição. A seguir uma tabela indicando qual parâmetro deve ser informado em cada estratégia e uma listagem com uma breve explicação de todos parâmetros necessários:

.. list-table:: Propriedades de cada tipo de estratégia
   :widths: auto
   :header-rows: 1

   * - Tipo de Estratégia
     - Strategy
     - Origin
     - User
     - Products
     - Categories
     - Campaigns
   * - Offers Store
     - ✓
     - ✓
     - 
     - 
     - 
     -
   * - Offers Category
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - Offers User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Most Viewed Store
     - ✓
     - ✓
     - 
     - 
     -
     -
   * - Most Viewed Categories
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - Most Viewed Campaigns
     - ✓
     - ✓
     - 
     - 
     - 
     - ✓
   * - Most Viewed User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Most Viewed Products
     - ✓
     - ✓
     - 
     - ✓
     - 
     -
   * - Best Sellers Store
     - ✓
     - ✓
     - 
     - 
     - 
     -
   * - Best Sellers Categories
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - Best Sellers Campaigns
     - ✓
     - ✓
     - 
     - 
     - 
     - ✓
   * - Best Sellers User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Best Sellers Product
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - New Releases Store
     - ✓
     - ✓
     - 
     - 
     -
     -
   * - New Releases Categories
     - ✓
     - ✓
     - 
     - 
     - ✓
     -
   * - New Releases Campaigns
     - ✓
     - ✓
     - 
     - 
     - 
     - ✓
   * - New Releases User
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - New Releases Products
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Click History
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Navigation History
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Order History
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Cart Abandonment
     - ✓
     - ✓
     - ✓
     - 
     -
     -
   * - Bought Together
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Best Choice
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Similar Products
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Who Clicked Also Clicked
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Who Clicked Also Purchased
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Who Purchased Also Purchased
     - ✓
     - ✓
     - 
     - ✓
     -
     -
   * - Promoted Store
     - ✓
     - ✓
     - 
     - 
     - 
     -
