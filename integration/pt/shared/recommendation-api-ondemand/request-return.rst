.. _api-ondemand-request-return-page:

Retorno do Recommendation Request
=================================

O retorno de uma chamada ao Recommendation Request é uma lista de objetos, onde cada objetos é formado por 4 (quatro) propriedades, que são:
    
    1. Base Ids
    2. Base Items
    3. Recommendation Ids
    4. Recommendation Items


1. Base Ids
-----------

Retorna uma Lista de *String* que contém os **ids** da entidade utilizada como **bases** para a requisição da recomendação, podendo ser id de produto, id de categoria e id de campanha. Vale ressaltar que esse campo pode ser nulo quando não existir nenhum id base (exemplo: requisição de recomenda com estratégias baseadas para a loja).

- *Exemplo 1:* estratégia "best sellers campaigns" vai possuir os ids das campanhas informadas na lista de baseIds.

- *Exemplo 2:* estratégia "best sellers categories" vai possuir os ids das categorias informadas na lista de baseIds.

- *Exemplo 3:* id do produto informado como parâmetro na pagina de detalhe de produto. 

- *Exemplo 4:* ids dos produtos clicados pelo usuário utilizado na estratégia "navigation history".


2. Base Items
-------------

Retorna uma Lista de **Itens de Recomendação** utilizados como bases para recomendação. Cada **item de recomendação** contém todas as informações do produto base. Esse campo pode ser vazio quando não existe nenhum produto base (exemplo: estratégia "Novidades da Categoria").

- *Exemplo 1:* produto informado como parâmetro na página de detalhe de produto.

- *Exemplo 2:* uma lista de produtos clicados pelo usuário utilizado na estratégia "navigation history". 


3. Recommendation Ids
---------------------

Retorna uma Lista de *String* que contém os **ids** dos **Itens de Recomendação**, que seriam os ids dos produtos recomendados.

- *Exemplo:* estratégia "best sellers campaigns" vai retornar uma lista de Ids de produtos mais vendidos para a campanha base.


4. Recommendation Items
-----------------------

Retorna uma Lista de **Itens de Recomendação**, que seriam os produtos recomendados.

- *Exemplo:* estratégia "best sellers campaigns" vai retornar uma lista de itens de recomendação dos produtos mais vendido para a campanha base. 


.. note:: O preenchimento do **baseIds** ou **baseItems**, **recommendationIds** ou **recommendationItems** varia de acordo com a configuração de retorno configurado pela Biggy. Se você desejar montar as recomendações utilizando os dados do seu banco de dados, por exemplo, é possível desabilitar o retorno do recommendationItems para diminuir a quantidade de dados trafegados, retornando somente os Ids de produtos que devem ser recomendados.



Item de Recomendação
--------------------

O **Item de Recomendação** é um objeto contido nas listas *Base Items* e *Recommendation Items* (já que ambas possuem a mesma estrutura de retorno). Cada item de recomendação possui os seguintes atributos:

- **productId** contendo o ID do produto.
- **score** contendo o score obtido pela recomendação (se houver). Ex.: estratégia best choice retorna um % indicando o quão relevante é a recomendação.
- **specs** contendo a lista de especificações para esse produto. Ex.: todas as cores e tamanhos de um tênis de corrida; todos os sabores de um whey protein.
- **offers** contendo a lista de SKUs do produto.

Os objetos existentes na lista de specs tem como objetivo apresentar as especificações do produtos utilizando a seguinte estrutura:

- **id** contendo o ID da spec.
- **label** contendo o rótulo/nome da spec. 
- **type** contendo o tipo da spec. Os possíveis valores são:
    - color
    - size
    - flavor 
- **subSpecs** contendo possíveis especificações filhas (se houver). Ex.: Tenis da "spec type color" azul possui as "subspecs type size" 37, 38, 39, 40 e 41. 

Os objetos existentes na lista de **offers** podem ser considerados o principal objeto do retorno pois detalham todas as SKUs do produto. Para isso é utilizado a seguinte estrutura:

- **offerId** contendo o ID da oferta. 
- **sku** contendo a SKU da oferta. 
- **name** contendo o nome da oferta. 
- **url** contendo o url da oferta. 
- **imageUrl** contendo a url da imagem da oferta. 
- **currencySymbol** contendo o símbolo da moeda como texto seguindo a formatação da moeda local.  
- **price** contendo o preço da oferta como texto seguindo a formatação da moeda local.  
- **oldPrice** contendo o preço "de"da oferta como texto seguindo a formatação da moeda local.  
- **priceValue** contendo o preço da oferta como numérico.  
- **installment** contendo o parcelamento da oferta. 
- **hasDiscount** informando se a oferta possui ou não desconto. 
- **discountPercentage** informando o percentual da oferta (se houver). 
- **specs** informando as especificações únicas da SKU (ex.: Tenis de corrida "spec type color" azul e "spec type size" 41). 
