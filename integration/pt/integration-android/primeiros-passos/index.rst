﻿Primeiros passos
================

Com o objetivo de facilitar o processo de integração com aplicativos Android, a Biggy disponibiliza uma **SDK** (Software Development Kit) que abstrai chamadas em nossas APIs de Recomendação e Tracking. Com essa SDK é possível utilizar todos os recursos disponíveis para que seja feito o acompanhamento de navegação do usuário, bem como solicitação de recomendações em diversos contextos.

Tipos de SDK
------------

A Biggy disponibiliza dois tipos de SDK:

1. Versão compacta (*biggysdk.jar*)

2. Versão com dependência (*biggysdk-with-dependencies.jar*)

.. note:: Independente do tipo selecionado, a inclusão no projeto do aplicativo Android se dá da mesma maneira.

Versão compacta
---------------

A versão compacta da SDK contém apenas as classes e chamadas desenvolvidas pela Biggy. Ela é uma versão com um tamanho menor, todavia para seu funcionamento, é imprescindível adicionar as dependências necessárias no **build.gradle** do seu aplicativo Android. É uma opção para aqueles que desejam manter um controle das versões das dependências do lado do aplicativo ou para aqueles que já possuem essas depêndencias em seu *build.gradle*, de modo que evita conflitos de incompatibilidade de versões. São elas:
- Gson
- Google Inject
- OkHttp

Dependências:

.. code-block:: gradle

    //GSON:
    compile 'com.google.code.gson:gson:2.7'
    //Google Inject:
    compile group: 'com.google.inject', name: 'guice', version: '4.0'
    //OkHttp:
    compile 'com.squareup.okhttp:okhttp:2.2.0'


Versão com dependência
----------------------

A versão com dependência da SDK já possui internamente as dependências mencionadas anteriormente no *.jar*. É uma versão com tamanho maior e é indicada para quem deseja não alterar ou adicionar outras dependências no **build.gradle** do seu aplicativo Android.

Inclusão da SDK em um aplicativo Android
----------------------------------------

A Biggy SDK é uma biblioteca de extensão .jar que deve ser incluída nas dependências do aplicativo Android.
A seguir um passo a passo simples para inclusão da dependência utilizando a IDE Android Studio:

1. Copiar o .jar para "<nome do aplicativo>/app/libs/".
2. Abrir a opção "Project Sctructure -> Dependencies", clicar na adição de um "Jar Dependency".

Configuração das chaves da Biggy
--------------------------------

Para o funcionamento da integração é necessário adicionar no **build.gradle** algumas variáveis de ambiente que são utilizadas nas requisições para as APIs de Tracking e Recomendação. 

.. image:: add-dependency-01.png

.. image:: add-dependency-02.png


Essas variáveis serão entregues pela Biggy e devem ser adicionadas da seguinte maneira no **build.gradle**:

.. code-block:: json

    buildTypes {
        debug {
            buildConfigField "String", "BIGGY_API_HOST", "<biggy_api_host_value>"
            buildConfigField "String", "BIGGY_API_KEY", "<biggy_api_key_value>"
            buildConfigField "String", "BIGGY_SECRET_KEY", "<biggy_secret_key_value>"
        }
        release {
            buildConfigField "String", "BIGGY_API_HOST", "<biggy_api_host_value>"
            buildConfigField "String", "BIGGY_API_KEY", "<biggy_api_key_value>"
            buildConfigField "String", "BIGGY_SECRET_KEY", "<biggy_secret_key_value>"
        }
    }

Para obter os valores basta realizar a chamada utilizando a classe **BuildConfig**:

.. code-block:: java

    final String apiHost = BuildConfig.BIGGY_API_HOST;
    final String apiKey = BuildConfig.BIGGY_API_KEY;
