﻿Introdução
**********

Nesta seção, o objetivo é prover todas as informações necessárias sobre a arquitetura do sistema, o fluxo dos dados e os passos necessários para integrar e utilizar os serviços fornecidos pela Biggy em um sistema Android.
