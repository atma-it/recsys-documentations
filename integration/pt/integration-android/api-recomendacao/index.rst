﻿API de recomendação para Android
================================

A Biggy SDK fornece recomendações personalizadas com uma variedade de **estratégias** que utilizam **parâmetros** para processar e retornar uma lista de **itens de recomendação**. 

Para mais detalhes sobre as possibilidades de retorno, acessar a seção :ref:`Características da Recomendação <api-ondemand-features-page>`

As chamadas para as APIs de Track e Recomendação são realizadas a partir do objeto **BiggyClient**, objeto responsável por montar o corpo da requisição e parsear o retorno obtido. Para instancia-lo, basta utilizar as variáveis de ambiente definidas no **build.gradle** e o **apllicationContext** da sua aplicação:

.. code-block:: java

    final String apiHost = BuildConfig.BIGGY_API_HOST;
    final String apiKey = BuildConfig.BIGGY_API_KEY
    
    biggyClient = new BiggyClient(getApplicationContext(), apiHost, apiKey);


.. warning:: Se essa instância for chamada em diversas classes, é indicado instanciar o objeto BiggyClient uma única vez e reutilizá-lo em toda a sua aplicação.

Essa API consiste em três métodos principais descritos abaixo:
    - Recommendation Request
    - Recommendation View
    - Recommendation Click

    
Recommendation Request
----------------------

O objeto **RecommendationMetadata** é responsável por encapsular todos os dados que serão submetidos para a API de recomendação e é obrigatório em todas as chamadas. A seguir a sua estrutura:

.. code-block:: java

    public class RecommendationMetadata {
        Strategy strategy;
        OriginType origin;
        String user;
        List<String> products;
        List<String> categories;
        Integer minProducts;
        Integer maxProducts;
        Boolean mergeRecommendationList;
        Boolean mixMergedRecommendations;
    
        // getters e setters
    }

    
Para mais detalhes sobre as propriedades (parâmetros), acessar a seção :ref:`Propriedades do Recommendation Request <api-ondemand-request-properties-page>` 


O método **recommendation** da classe **BiggyClient** é responsável por realizar a chamada a API. Esse método recebe um **RecommendationMetadata** como parâmetro e retorna uma *lista de Recommendation*, objeto que possui a seguinte estrutura:

.. code-block:: java

    public class Recommendation {
        List<String> baseIds;
        List<ProductRecommendation> basesItems;
        List<String> recommendationIds;
        List<ProductRecommendation> recommendationItems;
        
        // getters e setters
    }
    
    public class ProductRecommendation {
        String productId;
        Double score;
        List<ProductRecommendationOffer> offers;
        Set<ProductSpec.Spec> specs;
        
        // getters e setters
    }

    public class ProductRecommendationOffer {
        String offerId;
        String originalProductId;
        String sku;
        String distributionCenter;
        String name;
        String url;
        String imageUrl;
        String secondaryImageUrl;
        Double price;
        Double oldPrice;
        String currencySymbol;
        Map<String, Double> installment;
        boolean hasDiscount;
        float discountPercentage;
        Map<String, String> extraInfo;
        Double score;
        Set<ProductSpec.Spec> specs;
        List<ProductRecommendationCategory> categories;
        
        // getters e setters
    }

    public class ProductSpec extends Entity {
        public static class Spec  {
            public enum TYPE {
                COLOR(new String[]{"cor", "color"}),
                SIZE(new String[]{"tamanho", "size"}),
                FLAVOR(new String[]{"sabor", "flavor"});
            }
        }
        
        private String id;
        private String label;
        private TYPE type;
        private String offerId;
        private Map<String, String> images;
        private Map<String, String> imagesSsl;
        private List<Spec> subSpecs;
        
        // getters e setters
    }
    
    public class ProductRecommendationCategory extends BaseEntity {
        private String storeId;
        private String name;
        private String parent;
        private String originalId;
        private List<String> ancestors;
        
        // getters e setters
    }
    
Desta forma, o retorno do Recommendation é composto por 4 listas:

    1. baseIds
    2. baseItems
    3. recommendationIds
    4. recommendationItems
    
Para mais detalhes sobre o retorno, acessar a seção :ref:`Retorno do Recommendation Request <api-ondemand-request-return-page>` 

A seguir um exemplo da chamada da recomendação:

.. code-block:: java

    biggyClient.recommendation(recommendationMetadata, new BiggyRecommendationListener() {
        @Override
        public void onRecommendation(List<Recommendation> recommendations) {
            if (recommendations != null) {
                // A lista de Recommendation possui todas as recomendações
            } 
        }
    });

Com a lista de bases e items de recomendação é possível montar as vitrines de acordo com a necessidade da sua app, utilizando todas as estratégias de recomendação e personalização disponibilizadas pela Biggy.


Recommendation View
-------------------

O método **recommendationView** presente no *BiggyClient* permite avaliar por meio do Dashboard da Biggy o desempenho da recomendação requisitada em relação à sua visualização. O objetivo é efetuar a chamada desse método quando o usuário "ver" a recomendação. Vale ressaltar que esse método realiza a chamada  na API da Biggy de forma assíncrona, não afetando a performance da aplicação.

A seguir um exempo da chamada do recommendationView:

.. code-block:: java

    biggyClient.recommendationView(recommendationViewMetadata);
    
O objeto **RecommendationViewMetadata** é responsável por encapsular todos os dados que serão submetidos na chamada do *recommendationView*. Os campos informados são obrigatórios em todas as chamadas. A seguir a sua estrutura e uma breve descrição de cada campo:

.. code-block:: java

    public class RecommendationViewMetadata {
        Strategy strategy;
        OriginType origin;
        String session;
        
        // getters e setters
    }

.. note:: Os atributos *strategy* e *origin* seguem a mesma estrutura existente para o Recommendation Request. Para mais detalhes, acessar as seções :ref:`Estratégias <api-strategy-page>` e :ref:`Origem <api-origin-page>`

Recommendation Click
--------------------

O método **recommendationClick** presente no *BiggyClient* permite avaliar por meio do Dashboard da Biggy o desempenho da recomendação requisitada em relação ao disparo de eventos de clique. O objetivo é efetuar a chamada desse método quando o usuário "clicar" em algum produto da recomendação. Vale ressaltar que esse método realiza a chamada na API da Biggy de forma assíncrona, não afetando a performance da aplicação.

A seguir um exempo da chamada do recommendationClick:

.. code-block:: java
    
    biggyClient.recommendationClick(recommendationClickMetadata);

O objeto **RecommendationClickMetadata** é responsável por encapsular todos os dados que serão submetidos na chamada do *recommendationClick*. Os campos informados são obrigatórios em todas as chamadas. A seguir a sua estrutura e uma breve descrição de cada campo:

.. code-block:: java

    public class RecommendationClickMetadata {
        Strategy strategy;
        OriginType origin;
        String session;
        String productId;
        
        // getters e setters
    }

.. note:: Os atributos *strategy* e *origin* seguem a mesma estrutura existente para o Recommendation Request. Para mais detalhes, acessar as seções :ref:`Estratégias <api-strategy-page>` e :ref:`Origem <api-origin-page>`

.. note:: O atributo **productId** é uma *String* e deve informar qual o Id do produto que o usuário clicou na recomendação. **É um atributo obrigatório.**




.. note:: Caso prefira realizar as chamadas de recomendação direto dede outra maneira, é possível requisitar as recomendações chamando diretamente de sua aplicação a API de recomendação da Biggy, com retorno no formato JSON, todavia **é obrigatório** a realização da chamadas dos três métodos (Recommendation Request, Recommendation View, Recommendation Click) caso prefira essa opção. Note que realizar a chamada dessa maneira **não substitui** também a implementação da integração do track. Para mais informações, consulte a equipe da Biggy.
